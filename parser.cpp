//
//  parser.cpp
//  Datapath Generator
//
//  Created by Brady Thomas on 10/28/20.
//

#include "parser.hpp"
#include <fstream>
#include <iostream>
#include <vector>
#include "dpgen.hpp"
#include <string>
#include <sstream>
#include <regex>

int parseFile(const char *filename) {
    std::ifstream inFile; 
    inFile.open(filename,fstream::in);

    int fileCount = 0;
    std::vector<connectionType> Connections;
    std::vector<component> Components;
    std::string str2Write;
    std::string str;
    int checkValue = 0;
    
    // Create connections for Clk and Rst
    connectionType connection;
    connection.Size = "Control";
    connection.Name = "Clk";
    connection.output = false;
    connection.input = true;
    connection.wire = false;
    connection.sign = 1;
    Connections.push_back(connection);
    connection.Size = "Control";
    connection.Name = "Rst";
    connection.output = false;
    connection.input = true;
    connection.wire = false;
    connection.sign = 1;
    Connections.push_back(connection);
    
    while(std::getline(inFile, str)) {
        checkValue = evaluateString(str, &Connections, &Components);
        fileCount++;
        if(checkValue != 0) {
            return -1;
        }
        
    }
    writeVerilog(&Connections, &Components, fileCount);
    
    
    return 0;
    
}

// And if there are size mismatches for inputs to do padding and correct datawidth
// Determine sizing for outputs and for datawidth to be appropriate
int evaluateString(std::string& line, std::vector<connectionType> *verilogConnections, std::vector<component> *verilogComponents) {
    int count = 0;
    std::string tempName;
    std::string tempSize;
    std::string const delims{ " ," };
    size_t beg, pos = 0;
    std::vector<std::string> strings;
    std::string tempStr;
    std::string tempStr2;
    std::string tempStr3;
    std::string tempStr4;
    std::string str2Write;
    std::string compWireSize;
    
    std::vector<std::string> tempInputs;
    std::vector<std::string> tempOutputs;
    
    std::vector<connectionType>::iterator it;
    component assignC;
    connectionType resizeW;
    int checker = 0;

    
    
    // Assign Inputs
    if((line.find("input Int") != std::string::npos) || (line.find("input UInt") != std::string::npos))  {
        connectionType connection;
        if (line.find("input Int") != std::string::npos) {
            line.erase(line.begin(), line.begin()+9);
            connection.sign = -1;
        }
        else if (line.find("input UInt") != std::string::npos) {
            line.erase(line.begin(), line.begin()+10);
            connection.sign = 1;
        }
        if (line.find("//") != std::string::npos) {
            std::cout << "Error Detected" << std::endl;
            return -1;
        }
        
        std::cout << line << std::endl;
        
        while((beg = line.find_first_not_of(delims, pos)) != std::string::npos){
            pos = line.find_first_of(delims, beg+1);
            if(count == 0) {
                tempSize.assign(line.substr(beg, pos-beg));
            }
            else {
                connection.Size = tempSize;
                connection.Name = line.substr(beg, pos-beg);
                connection.output = false;
                connection.input = true;
                connection.wire = false;
                connection.regist = false;
                verilogConnections->push_back(connection);
                std::cout << verilogConnections->size() << std::endl;
            }
            count++;
        }
        count = 0;
    }
    // Assign Outputs
    else if((line.find("output Int") != std::string::npos) || line.find("output UInt") != std::string::npos) {
        connectionType connection;
        if (line.find("output Int") != std::string::npos) {
            line.erase(line.begin(), line.begin()+10);
            connection.sign = -1;
        }
        else if (line.find("output UInt") != std::string::npos) {
            line.erase(line.begin(), line.begin()+11);
            connection.sign = 1;
        }
        if(line.find("//") != std::string::npos) {
            std::cout << "Error Detected" << std::endl;
            return -1;
        }
        std::cout << line << std::endl;
        
        while((beg = line.find_first_not_of(delims, pos)) != std::string::npos) {
            pos = line.find_first_of(delims, beg+1);
            if(count == 0) {
                tempSize.assign(line.substr(beg, pos-beg));
            }
            else {
                connection.Size = tempSize;
                connection.Name = line.substr(beg, pos-beg);
                connection.output = true;
                connection.input = false;
                connection.wire = false;
                connection.regist = false;
                verilogConnections->push_back(connection);
                std::cout << verilogConnections->size() << std::endl;
            }
            count++;
        }
        count = 0;
    }
    
    // Assign wires
    else if((line.find("wire Int") != std::string::npos) || (line.find("wire UInt") != std::string::npos)) {
        connectionType connection;
        if (line.find("wire Int") != std::string::npos) {
            line.erase(line.begin(), line.begin()+8);
            connection.sign = -1;
        }
        else if (line.find("wire UInt") != std::string::npos) {
            line.erase(line.begin(), line.begin()+9);
            connection.sign = 1;
        }
        if(line.find("//") != std::string::npos) {
            std::cout << "Error Detected" << std::endl;
            return -1;
        }
        std::cout << line << std::endl;
        while((beg = line.find_first_not_of(delims, pos)) != std::string::npos) {
            pos = line.find_first_of(delims, beg+1);
            if(count == 0) {
                tempSize.assign(line.substr(beg, pos-beg));
            }
            else {
                connection.Size = tempSize;
                connection.Name = line.substr(beg, pos-beg);
                connection.output = false;
                connection.input = false;
                connection.wire = true;
                connection.regist = false;
                verilogConnections->push_back(connection);
                std::cout << verilogConnections->size() << std::endl;
            }
            count++;
        }
        count = 0;
    }
    
    // Assign Registers
    else if((line.find("register Int") != std::string::npos) || (line.find("register UInt") != std::string::npos)) {
        connectionType connection;
        if (line.find("register Int") != std::string::npos) {
            line.erase(line.begin(), line.begin()+12);
            connection.sign = -1;
        }
        else if (line.find("register UInt") != std::string::npos) {
            line.erase(line.begin(), line.begin()+13);
            connection.sign = 1;
        }
        if(line.find("//") != std::string::npos) {
            std::cout << "Error Detected" << std::endl;
            return -1;
        }
        std::cout << line << std::endl;
        while((beg = line.find_first_not_of(delims, pos)) != std::string::npos) {
            pos = line.find_first_of(delims, beg+1);
            if(count == 0) {
                tempSize.assign(line.substr(beg, pos-beg));
            }
            else {
                
                connection.Size = tempSize;
                connection.Name = line.substr(beg, pos-beg);
                connection.output = false;
                connection.input = false;
                connection.wire = false;
                connection.regist = true;
                verilogConnections->push_back(connection);
                std::cout << verilogConnections->size() << std::endl;
            }
            count++;
        }
        count = 0;
    }
    
    
    // Assign components
    else if(line.find("=") != std::string::npos) {
        component c1;
        // Adder
        if((line.find("+") != std::string::npos) && (!(line.find("+ 1") != std::string::npos))) {
            if(line.find("//") != std::string::npos) {
                std::cout << "Error Detected" << std::endl;
                return -1;
            }
            c1.compName = "ADD";
            while((beg = line.find_first_not_of(delims, pos)) != std::string::npos) {
                pos = line.find_first_of(delims, beg+1);
                strings.push_back(line.substr(beg, pos-beg));
            }
            tempStr = strings.at(0);
            tempStr2 = strings.at(2);
            tempStr3 = strings.at(4);
            // Find connection
            for(count = 0; count < verilogConnections->size(); count++) {
                if(verilogConnections->at(count).Name.compare(tempStr) == 0) {
                    tempSize = verilogConnections->at(count).getSize();
                    c1.size = tempSize;
                    c1.latency = setLatencyValues(tempSize, c1.compName);
                    c1.componentOutputs.push_back(verilogConnections->at(count));
                }
                else if(verilogConnections->at(count).Name.compare(tempStr2) == 0) {
                    c1.componentInputs.push_back(verilogConnections->at(count));
                }
                else if(verilogConnections->at(count).Name.compare(tempStr3) == 0) {
                    c1.componentInputs.push_back(verilogConnections->at(count));
                }
                
                // Check for signed or unsigned
                if(verilogConnections->at(count).sign == -1) {
                    c1.sign = -1;
                }

                    
            }
            
            checker = checkSize(&c1, &assignC, &resizeW);
            if (checker == 1) {
                verilogConnections->push_back(resizeW);
                assignC.componentOutputs.push_back(resizeW);
                c1.componentInputs.erase(c1.componentInputs.begin());
                c1.componentInputs.insert(c1.componentInputs.begin(), resizeW);
                verilogComponents->push_back(assignC);
            }
            verilogComponents->push_back(c1);
        }
        // Subtractor
        else if((line.find("-") != std::string::npos) && (!(line.find("- 1") != std::string::npos))) {
            if(line.find("//") != std::string::npos) {
                std::cout << "Error Detected" << std::endl;
                return -1;
            }
            c1.compName = "SUB";
            while((beg = line.find_first_not_of(delims, pos)) != std::string::npos) {
                pos = line.find_first_of(delims, beg+1);
                strings.push_back(line.substr(beg, pos-beg));
            }
            tempStr = strings.at(0);
            tempStr2 = strings.at(2);
            tempStr3 = strings.at(4);
            for(count = 0; count < verilogConnections->size(); count++) {
                if(verilogConnections->at(count).Name.compare(tempStr) == 0) {
                    tempSize = verilogConnections->at(count).getSize();
                    c1.size = tempSize;
                    c1.latency = setLatencyValues(tempSize, c1.compName);
                    c1.componentOutputs.push_back(verilogConnections->at(count));
                }
                else if(verilogConnections->at(count).Name.compare(tempStr2) == 0) {
                    c1.componentInputs.push_back(verilogConnections->at(count));
                }
                else if(verilogConnections->at(count).Name.compare(tempStr3) == 0) {
                    c1.componentInputs.push_back(verilogConnections->at(count));
                }
                
                // Check for signed or unsigned
                if(verilogConnections->at(count).sign == -1) {
                    c1.sign = -1;
                }
                
                
            }
            checker = checkSize(&c1, &assignC, &resizeW);
            if (checker == 1) {
                verilogConnections->push_back(resizeW);
                assignC.componentOutputs.push_back(resizeW);
                c1.componentInputs.erase(c1.componentInputs.begin());
                c1.componentInputs.insert(c1.componentInputs.begin(), resizeW);
                verilogComponents->push_back(assignC);
            }
            verilogComponents->push_back(c1);
        }
        // Multiplier
        else if(line.find("*") != std::string::npos) {
            if(line.find("//") != std::string::npos) {
                std::cout << "Error Detected" << std::endl;
                return -1;
            }
            c1.compName = "MUL";
            while((beg = line.find_first_not_of(delims, pos)) != std::string::npos) {
                pos = line.find_first_of(delims, beg+1);
                strings.push_back(line.substr(beg, pos-beg));
            }
            tempStr = strings.at(0);
            tempStr2 = strings.at(2);
            tempStr3 = strings.at(4);
            for(count = 0; count < verilogConnections->size(); count++) {
                if(verilogConnections->at(count).Name.compare(tempStr) == 0) {
                    tempSize = verilogConnections->at(count).getSize();
                    c1.size = tempSize;
                    c1.latency = setLatencyValues(tempSize, c1.compName);
                    c1.componentOutputs.push_back(verilogConnections->at(count));
                }
                else if(verilogConnections->at(count).Name.compare(tempStr2) == 0) {
                    c1.componentInputs.push_back(verilogConnections->at(count));
                }
                else if(verilogConnections->at(count).Name.compare(tempStr3) == 0) {
                    c1.componentInputs.push_back(verilogConnections->at(count));
                }
                
                // Check for signed or unsigned
                if(verilogConnections->at(count).sign == -1) {
                    c1.sign = -1;
                }
                
            }
            checker = checkSize(&c1, &assignC, &resizeW);
            if (checker == 1) {
                verilogConnections->push_back(resizeW);
                assignC.componentOutputs.push_back(resizeW);
                c1.componentInputs.erase(c1.componentInputs.begin());
                c1.componentInputs.insert(c1.componentInputs.begin(), resizeW);
                verilogComponents->push_back(assignC);
            }
            verilogComponents->push_back(c1);
        }
        // Comparator (GT output)
       else if(line.find(" > ") != std::string::npos) {
           if(line.find("//") != std::string::npos) {
               std::cout << "Error Detected" << std::endl;
               return -1;
           }
           c1.compName = "COMP";
            while((beg = line.find_first_not_of(delims, pos)) != std::string::npos) {
                pos = line.find_first_of(delims, beg+1);
                strings.push_back(line.substr(beg, pos-beg));
            }
            tempStr = strings.at(0);
            tempStr2 = strings.at(2);
            tempStr3 = strings.at(4);
            for(count = 0; count < verilogConnections->size(); count++) {
                if(verilogConnections->at(count).Name.compare(tempStr) == 0) {
                    compWireSize = verilogConnections->at(count).getSize();
                    c1.latency = setLatencyValues(tempSize, c1.compName);
                    c1.componentOutputs.push_back(verilogConnections->at(count));
                }
                else if(verilogConnections->at(count).Name.compare(tempStr2) == 0) {
                    c1.componentInputs.push_back(verilogConnections->at(count));
                    tempSize = verilogConnections->at(count).getSize();
                    c1.size = tempSize;
                }
                else if(verilogConnections->at(count).Name.compare(tempStr3) == 0) {
                    c1.componentInputs.push_back(verilogConnections->at(count));
                }
                
                // Check for signed or unsigned
                if(verilogConnections->at(count).sign == -1) {
                    c1.sign = -1;
                }
                
                
            }
           connectionType conn1;
           conn1.Name.assign("LT");
           conn1.Size.assign(compWireSize);
           conn1.input = false;
           conn1.output = false;
           conn1.wire = true;
           conn1.regist = false;
           c1.componentOutputs.push_back(conn1);
           verilogConnections->push_back(conn1);
           connectionType conn2;
           conn2.Name.assign("EQ");
           conn2.Size.assign(compWireSize);
           conn2.input = false;
           conn2.output = false;
           conn2.wire = true;
           conn2.regist = false;
           c1.componentOutputs.push_back(conn2);
           verilogConnections->push_back(conn2);
           checker = checkSize(&c1, &assignC, &resizeW);
           if (checker == 1) {
               verilogConnections->push_back(resizeW);
               assignC.componentOutputs.push_back(resizeW);
               c1.componentInputs.erase(c1.componentInputs.begin());
               c1.componentInputs.insert(c1.componentInputs.begin(), resizeW);
               verilogComponents->push_back(assignC);
           }
           verilogComponents->push_back(c1);
        }
        // Comparator (LT output)
        else if(line.find(" < ") != std::string::npos) {
            if(line.find("//") != std::string::npos) {
                std::cout << "Error Detected" << std::endl;
                return -1;
            }
            c1.compName = "COMP";
            connectionType conn1;
            conn1.Name.assign("GT");
            conn1.Size.assign(compWireSize);
            conn1.input = false;
            conn1.output = false;
            conn1.wire = true;
            conn1.regist = false;
            c1.componentOutputs.push_back(conn1);
            while((beg = line.find_first_not_of(delims, pos)) != std::string::npos) {
                pos = line.find_first_of(delims, beg+1);
                strings.push_back(line.substr(beg, pos-beg));
            }
            tempStr = strings.at(0);
            tempStr2 = strings.at(2);
            tempStr3 = strings.at(4);
            for(count = 0; count < verilogConnections->size(); count++) {
                if(verilogConnections->at(count).Name.compare(tempStr) == 0) {
                    compWireSize = verilogConnections->at(count).getSize();
                    c1.latency = setLatencyValues(tempSize, c1.compName);
                    c1.componentOutputs.push_back(verilogConnections->at(count));
                }
                else if(verilogConnections->at(count).Name.compare(tempStr2) == 0) {
                    tempSize = verilogConnections->at(count).getSize();
                    c1.size = tempSize;
                    c1.componentInputs.push_back(verilogConnections->at(count));
                }
                else if(verilogConnections->at(count).Name.compare(tempStr3) == 0) {
                    c1.componentInputs.push_back(verilogConnections->at(count));
                }
                
                // Check for signed or unsigned
                if(verilogConnections->at(count).sign == -1) {
                    c1.sign = -1;
                }
                
                
            }
            connectionType conn2;
            conn2.Name.assign("EQ");
            conn2.Size.assign(compWireSize);
            conn2.input = false;
            conn2.output = false;
            conn2.wire = true;
            conn2.regist = false;
            c1.componentOutputs.push_back(conn2);
            checker = checkSize(&c1, &assignC, &resizeW);
            if (checker == 1) {
                verilogConnections->push_back(resizeW);
                assignC.componentOutputs.push_back(resizeW);
                c1.componentInputs.erase(c1.componentInputs.begin());
                c1.componentInputs.insert(c1.componentInputs.begin(), resizeW);
                verilogComponents->push_back(assignC);
            }
            verilogComponents->push_back(c1);
        }
        // Comparator (EQ output)
        else if(line.find("==") != std::string::npos) {
            if(line.find("//") != std::string::npos) {
                std::cout << "Error Detected" << std::endl;
                return -1;
            }
            c1.compName = "COMP";
            connectionType conn1;
            conn1.Name.assign("GT");
            conn1.Size.assign(compWireSize);
            conn1.input = false;
            conn1.output = false;
            conn1.wire = true;
            conn1.regist = false;
            it = c1.componentOutputs.begin();
            c1.componentOutputs.push_back(conn1);
            connectionType conn2;
            conn2.Name.assign("LT");
            conn2.Size.assign(compWireSize);
            conn2.input = false;
            conn2.output = false;
            conn2.wire = true;
            conn2.regist = false;
            c1.componentOutputs.push_back(conn2);
            while((beg = line.find_first_not_of(delims, pos)) != std::string::npos) {
                pos = line.find_first_of(delims, beg+1);
                strings.push_back(line.substr(beg, pos-beg));
            }
            tempStr = strings.at(0);
            tempStr2 = strings.at(2);
            tempStr3 = strings.at(4);
            for(count = 0; count < verilogConnections->size(); count++) {
                if(verilogConnections->at(count).Name.compare(tempStr) == 0) {
                    compWireSize = verilogConnections->at(count).getSize();
                    c1.latency = setLatencyValues(tempSize, c1.compName);
                    c1.componentOutputs.push_back(verilogConnections->at(count));
                }
                else if(verilogConnections->at(count).Name.compare(tempStr2) == 0) {
                    tempSize = verilogConnections->at(count).getSize();
                    c1.size = tempSize;
                    c1.componentInputs.push_back(verilogConnections->at(count));
                }
                else if(verilogConnections->at(count).Name.compare(tempStr3) == 0) {
                    c1.componentInputs.push_back(verilogConnections->at(count));
                }
                
                // Check for signed or unsigned
                if(verilogConnections->at(count).sign == -1) {
                    c1.sign = -1;
                }
            }
            checker = checkSize(&c1, &assignC, &resizeW);
            if (checker == 1) {
                verilogConnections->push_back(resizeW);
                assignC.componentOutputs.push_back(resizeW);
                c1.componentInputs.erase(c1.componentInputs.begin());
                c1.componentInputs.insert(c1.componentInputs.begin(), resizeW);
                verilogComponents->push_back(assignC);
            }
            verilogComponents->push_back(c1);
        }
        // MUX2x1
        else if(line.find("?") != std::string::npos) {
            if(line.find("//") != std::string::npos) {
                std::cout << "Error Detected" << std::endl;
                return -1;
            }
            c1.compName = "MUX2x1";
            while((beg = line.find_first_not_of(delims, pos)) != std::string::npos) {
                pos = line.find_first_of(delims, beg+1);
                strings.push_back(line.substr(beg, pos-beg));
            }
            tempStr = strings.at(0);
            tempStr2 = strings.at(2);
            tempStr3 = strings.at(4);
            tempStr4 = strings.at(6);
            for(count = 0; count < verilogConnections->size(); count++) {
                if(verilogConnections->at(count).Name.compare(tempStr) == 0) {
                    tempSize = verilogConnections->at(count).getSize();
                    c1.size = tempSize;
                    c1.latency = setLatencyValues(tempSize, c1.compName);
                    c1.componentOutputs.push_back(verilogConnections->at(count));
                }
                else if(verilogConnections->at(count).Name.compare(tempStr2) == 0) {
                    c1.componentInputs.push_back(verilogConnections->at(count));
                }
                else if(verilogConnections->at(count).Name.compare(tempStr3) == 0) {
                    c1.componentInputs.push_back(verilogConnections->at(count));
                }
                else if(verilogConnections->at(count).Name.compare(tempStr4) == 0) {
                    c1.componentInputs.push_back(verilogConnections->at(count));
                }
                
                // Check for signed or unsigned
                if(verilogConnections->at(count).sign == -1) {
                    c1.sign = -1;
                }
                
            }
            verilogComponents->push_back(c1);
        }
        // SHR
        else if(line.find(">>") != std::string::npos) {
            if(line.find("//") != std::string::npos) {
                std::cout << "Error Detected" << std::endl;
                return -1;
            }
            c1.compName = "SHR";
            while((beg = line.find_first_not_of(delims, pos)) != std::string::npos) {
                pos = line.find_first_of(delims, beg+1);
                strings.push_back(line.substr(beg, pos-beg));
            }
            tempStr = strings.at(0);
            tempStr2 = strings.at(2);
            tempStr3 = strings.at(4);
            for(count = 0; count < verilogConnections->size(); count++) {
                if(verilogConnections->at(count).Name.compare(tempStr) == 0) {
                    tempSize = verilogConnections->at(count).getSize();
                    c1.size = tempSize;
                    c1.latency = setLatencyValues(tempSize, c1.compName);
                    c1.componentOutputs.push_back(verilogConnections->at(count));
                }
                else if(verilogConnections->at(count).Name.compare(tempStr2) == 0) {
                    c1.componentInputs.push_back(verilogConnections->at(count));
                }
                else if(verilogConnections->at(count).Name.compare(tempStr3) == 0) {
                    c1.componentInputs.push_back(verilogConnections->at(count));
                }
                
                // Check for signed or unsigned
                if(verilogConnections->at(count).sign == -1) {
                    c1.sign = -1;
                }
                
            }
            verilogComponents->push_back(c1);
        }
        // SHL
        else if(line.find("<<") != std::string::npos) {
            if(line.find("//") != std::string::npos) {
                std::cout << "Error Detected" << std::endl;
                return -1;
            }
            c1.compName = "SHL";
            while((beg = line.find_first_not_of(delims, pos)) != std::string::npos) {
                pos = line.find_first_of(delims, beg+1);
                strings.push_back(line.substr(beg, pos-beg));
            }
            tempStr = strings.at(0);
            tempStr2 = strings.at(2);
            tempStr3 = strings.at(4);
            for(count = 0; count < verilogConnections->size(); count++) {
                if(verilogConnections->at(count).Name.compare(tempStr) == 0) {
                    tempSize = verilogConnections->at(count).getSize();
                    c1.size = tempSize;
                    c1.latency = setLatencyValues(tempSize, c1.compName);
                    c1.componentOutputs.push_back(verilogConnections->at(count));
                }
                else if(verilogConnections->at(count).Name.compare(tempStr2) == 0) {
                    c1.componentInputs.push_back(verilogConnections->at(count));
                }
                else if(verilogConnections->at(count).Name.compare(tempStr3) == 0) {
                    c1.componentInputs.push_back(verilogConnections->at(count));
                }
                
                // Check for signed or unsigned
                if(verilogConnections->at(count).sign == -1) {
                    c1.sign = -1;
                }
                
            }
            verilogComponents->push_back(c1);
        }
        // DIV
        else if(line.find("/") != std::string::npos) {
            if(line.find("//") != std::string::npos) {
                std::cout << "Error Detected" << std::endl;
                return -1;
            }
            c1.compName = "DIV";
            while((beg = line.find_first_not_of(delims, pos)) != std::string::npos) {
                pos = line.find_first_of(delims, beg+1);
                strings.push_back(line.substr(beg, pos-beg));
            }
            tempStr = strings.at(0);
            tempStr2 = strings.at(2);
            tempStr3 = strings.at(4);
            for(count = 0; count < verilogConnections->size(); count++) {
                if(verilogConnections->at(count).Name.compare(tempStr) == 0) {
                    tempSize = verilogConnections->at(count).getSize();
                    c1.size = tempSize;
                    c1.latency = setLatencyValues(tempSize, c1.compName);
                    c1.componentOutputs.push_back(verilogConnections->at(count));
                }
                else if(verilogConnections->at(count).Name.compare(tempStr2) == 0) {
                    c1.componentInputs.push_back(verilogConnections->at(count));
                }
                else if(verilogConnections->at(count).Name.compare(tempStr3) == 0) {
                    c1.componentInputs.push_back(verilogConnections->at(count));
                }
                
                // Check for signed or unsigned
                if(verilogConnections->at(count).sign == -1) {
                    c1.sign = -1;
                }
                
            }
            verilogComponents->push_back(c1);
        }
        // MOD
        else if(line.find("%") != std::string::npos) {
            if(line.find("//") != std::string::npos) {
                std::cout << "Error Detected" << std::endl;
                return -1;
            }
            c1.compName = "MOD";
            while((beg = line.find_first_not_of(delims, pos)) != std::string::npos) {
                pos = line.find_first_of(delims, beg+1);
                strings.push_back(line.substr(beg, pos-beg));
            }
            tempStr = strings.at(0);
            tempStr2 = strings.at(2);
            tempStr3 = strings.at(4);
            for(count = 0; count < verilogConnections->size(); count++) {
                if(verilogConnections->at(count).Name.compare(tempStr) == 0) {
                    tempSize = verilogConnections->at(count).getSize();
                    c1.size = tempSize;
                    c1.latency = setLatencyValues(tempSize, c1.compName);
                    c1.componentOutputs.push_back(verilogConnections->at(count));
                }
                else if(verilogConnections->at(count).Name.compare(tempStr2) == 0) {
                    c1.componentInputs.push_back(verilogConnections->at(count));
                }
                else if(verilogConnections->at(count).Name.compare(tempStr3) == 0) {
                    c1.componentInputs.push_back(verilogConnections->at(count));
                }
                
                // Check for signed or unsigned
                if(verilogConnections->at(count).sign == -1) {
                    c1.sign = -1;
                }
                
            }
            verilogComponents->push_back(c1);
        }
        // INC
        else if(line.find("+ 1") != std::string::npos) {
            if(line.find("//") != std::string::npos) {
                std::cout << "Error Detected" << std::endl;
                return -1;
            }
            c1.compName = "INC";
            while((beg = line.find_first_not_of(delims, pos)) != std::string::npos) {
                pos = line.find_first_of(delims, beg+1);
                strings.push_back(line.substr(beg, pos-beg));
            }
            tempStr = strings.at(0);
            tempStr2 = strings.at(2);
            for(count = 0; count < verilogConnections->size(); count++) {
                if(verilogConnections->at(count).Name.compare(tempStr) == 0) {
                    tempSize = verilogConnections->at(count).getSize();
                    c1.size = tempSize;
                    c1.latency = setLatencyValues(tempSize, c1.compName);
                    c1.componentOutputs.push_back(verilogConnections->at(count));
                }
                else if(verilogConnections->at(count).Name.compare(tempStr2) == 0) {
                    c1.componentInputs.push_back(verilogConnections->at(count));
                }
                
                // Check for signed or unsigned
                if(verilogConnections->at(count).sign == -1) {
                    c1.sign = -1;
                }

                
            }
            // Create Wire for 1
            connectionType connection;
            connection.Size = "1";
            connection.Name = "1";
            connection.output = false;
            connection.input = false;
            connection.wire = true;
            verilogConnections->push_back(connection);
            c1.componentInputs.push_back(connection);
            verilogComponents->push_back(c1);
        }
        // DEC
        else if(line.find("- 1") != std::string::npos) {
            if(line.find("//") != std::string::npos) {
                std::cout << "Error Detected" << std::endl;
                return -1;
            }
            c1.compName = "DEC";
            while((beg = line.find_first_not_of(delims, pos)) != std::string::npos) {
                pos = line.find_first_of(delims, beg+1);
                strings.push_back(line.substr(beg, pos-beg));
            }
            tempStr = strings.at(0);
            tempStr2 = strings.at(2);
            for(count = 0; count < verilogConnections->size(); count++) {
                if(verilogConnections->at(count).Name.compare(tempStr) == 0) {
                    tempSize = verilogConnections->at(count).getSize();
                    c1.size = tempSize;
                    c1.latency = setLatencyValues(tempSize, c1.compName);
                    c1.componentOutputs.push_back(verilogConnections->at(count));
                }
                else if(verilogConnections->at(count).Name.compare(tempStr2) == 0) {
                    c1.componentInputs.push_back(verilogConnections->at(count));
                }
                
                // Check for signed or unsigned
                if(verilogConnections->at(count).sign == -1) {
                    c1.sign = -1;
                }
                
                
            }
            // Create Wire for 1
            connectionType connection;
            connection.Size = "1";
            connection.Name = "1";
            connection.output = false;
            connection.input = false;
            connection.wire = true;
            verilogConnections->push_back(connection);
            c1.componentInputs.push_back(connection);
            verilogComponents->push_back(c1);
        }
        
        // REG
        else {
            if(line.find("//") != std::string::npos) {
                std::cout << "Error Detected" << std::endl;
                return -1;
            }
            c1.compName = "REG";
            while((beg = line.find_first_not_of(delims, pos)) != std::string::npos) {
                pos = line.find_first_of(delims, beg+1);
                strings.push_back(line.substr(beg, pos-beg));
            }
            tempStr = strings.at(0);
            tempStr2 = strings.at(2);
            for(count = 0; count < verilogConnections->size(); count++) {
                if(verilogConnections->at(count).Name.compare(tempStr) == 0) {
                    tempSize = verilogConnections->at(count).getSize();
                    c1.size = tempSize;
                    c1.latency = setLatencyValues(tempSize, c1.compName);
                    c1.componentOutputs.push_back(verilogConnections->at(count));
                }
                else if(verilogConnections->at(count).Name.compare(tempStr2) == 0) {
                    c1.componentInputs.push_back(verilogConnections->at(count));
                }
                
                else if(verilogConnections->at(count).Name.compare("Clk") == 0) {
                    c1.componentInputs.push_back(verilogConnections->at(count));
                }
                
                else if(verilogConnections->at(count).Name.compare("Rst") == 0) {
                    c1.componentInputs.push_back(verilogConnections->at(count));
                }
                
                // Check for signed or unsigned
                if(verilogConnections->at(count).sign == -1) {
                    c1.sign = -1;
                }
                
                
            }
            verilogComponents->push_back(c1);
        }
    }
    
    return 0;
}


void writeVerilog(std::vector<connectionType> *finalConnections, std::vector<component> *finalComponents, int fileCount) {
    int a = 0;
    int b = 0;
    int c = 0;
    int tempSize = 0;
    std::vector<std::string> modVec;
    
    // Instance counter
    int reg = 1;
    int add = 1;
    int sub = 1;
    int mul = 1;
    int comp = 1;
    int mux = 1;
    int shr = 1;
    int shl = 1;
    int div = 1;
    int mod = 1;
    int inc = 1;
    int dec = 1;
    
    int signHelp = 0;
    
    std::ofstream outputfile(outputFile);
    
    if(!outputfile) {
        std::cout << "Error in creating file" << std::endl;
    }
    
    
    
    for(a = 0; a < 7; a++) {
        // Write time scale and module
        if (a == 0) {
            outputfile << "'timescale 1ns / 1ps" << std::endl;
            outputfile << std::endl;
            outputfile << "module circuit1(";
            // Collect names for module
            for(b = 0; b < finalConnections->size(); b++) {
                if(finalConnections->at(b).input == true) {
                    modVec.push_back(finalConnections->at(b).Name);
                }
                else if(finalConnections->at(b).output == true) {
                    modVec.push_back(finalConnections->at(b).Name);
                }
            }
            // Write them into verilog file
            for(c = 0; c < modVec.size(); c++) {
                if(c != modVec.size() - 1) {
                    outputfile << modVec.at(c) << ", ";
                }
                else {
                    outputfile << modVec.at(c) << ");" << std::endl;
                }
            }
        }
        
        // Write inputs
        else if(a == 1){
            for(b = 0; b < finalConnections->size(); b++) {
                if((finalConnections->at(b).input == true) && (finalConnections->at(b).Name.compare("Clk") != 0) && (finalConnections->at(b).Name.compare("Rst") != 0)) {
                    std::stringstream holder(finalConnections->at(b).Size);
                    holder >> tempSize;
                    if(tempSize == 1) {
                        outputfile << "input " << finalConnections->at(b).Name << ";" << std::endl;
                    }
                    else {
                        outputfile << "input [" << tempSize - 1 << ":0] " << finalConnections->at(b).Name << ";" << std::endl;
                    }
                }
            }
        }
        
        
        // Write control inputs
        else if(a == 2){
            outputfile << std::endl;
            for(b = 0; b < finalConnections->size(); b++) {
                if((finalConnections->at(b).input == true) && ((finalConnections->at(b).Name.compare("Clk") == 0) || (finalConnections->at(b).Name.compare("Rst") == 0))) {
                    outputfile << "input " << finalConnections->at(b).Name << ";" << std::endl;
                }
            }
        }
        
        // Write outputs
        else if(a == 3) {
            outputfile << std::endl;
            for(b = 0; b < finalConnections->size(); b++) {
                if(finalConnections->at(b).output == true) {
                    std::stringstream holder(finalConnections->at(b).Size);
                    holder >> tempSize;
                    outputfile << "output [" << tempSize - 1 << ":0] " << finalConnections->at(b).Name << ";" << std::endl;
                }
            }
        }
        
        // Write wires
        else if(a == 4) {
            outputfile << std::endl;
            for(b = 0; b < finalConnections->size(); b++) {
                if(finalConnections->at(b).wire == true) {
                    std::stringstream holder(finalConnections->at(b).Size);
                    holder >> tempSize;
                    if(tempSize == 1) {
                        outputfile << "wire " << finalConnections->at(b).Name << ";" << std::endl;
                    }
                    else {
                        outputfile << "wire [" << tempSize - 1 << ":0] " << finalConnections->at(b).Name << ";" << std::endl;
                    }
                    
                }
            }
        }
        
        // Write components
        else if(a == 5) {
            outputfile << std::endl;
            for(b = 0; b < finalComponents->size(); b++) {
                // Adder
                if(finalComponents->at(b).compName.compare("ADD") == 0) {
                    // Check for signed/unsigned
                    if(finalComponents->at(b).sign == -1) {
                        if((finalComponents->at(b).componentInputs.at(0).sign == -1) && (finalComponents->at(b).componentInputs.at(0).sign == -1)) {
                            outputfile << "S" << finalComponents->at(b).compName << " #(.DATAWIDTH(" << finalComponents->at(b).size << ")) ";
                            outputfile << "add_" << add << "$signed({" << finalComponents->at(b).componentInputs.at(0).Size << "'b0, " << finalComponents->at(b).componentInputs.at(0).Name;
                            outputfile << "}), " << "$signed({ " << finalComponents->at(b).componentInputs.at(1).Size << "'b0, ";
                            outputfile << finalComponents->at(b).componentOutputs.at(0).Name << ");" << std::endl;
                    }
                    
                    else {
                        outputfile << finalComponents->at(b).compName << " #(.DATAWIDTH(" << finalComponents->at(b).size << ")) ";
                        outputfile << "add_" << add << "(" << finalComponents->at(b).componentInputs.at(0).Name << ", " << finalComponents->at(b).componentInputs.at(1).Name;
                        outputfile << ", " << finalComponents->at(b).componentOutputs.at(0).Name << ");" << std::endl;
                    }
                        add++;
                    }
                }
                
                // Subtractor
                else if(finalComponents->at(b).compName.compare("SUB") == 0) {
                    outputfile << finalComponents->at(b).compName << " #(.DATAWIDTH(" << finalComponents->at(b).size << ")) ";
                    outputfile << "sub_" << sub << "(" << finalComponents->at(b).componentInputs.at(0).Name << ", " << finalComponents->at(b).componentInputs.at(1).Name;
                    outputfile << ", " << finalComponents->at(b).componentOutputs.at(0).Name << ");" << std::endl;
                    sub++;
                }
                
                // Multiplier
                else if(finalComponents->at(b).compName.compare("MUL") == 0) {
                    outputfile << finalComponents->at(b).compName << " #(.DATAWIDTH(" << finalComponents->at(b).size << ")) ";
                    outputfile << "mul_" << mul << "(" << finalComponents->at(b).componentInputs.at(0).Name << ", " << finalComponents->at(b).componentInputs.at(1).Name;
                    outputfile << ", " << finalComponents->at(b).componentOutputs.at(0).Name << ");" << std::endl;
                    mul++;
                }
                
                // Comparator
                else if(finalComponents->at(b).compName.compare("COMP") == 0) {
                    outputfile << finalComponents->at(b).compName << " #(.DATAWIDTH(" << finalComponents->at(b).size << ")) ";
                    outputfile << "comp_" << comp << "(" << finalComponents->at(b).componentInputs.at(0).Name << ", " << finalComponents->at(b).componentInputs.at(1).Name;
                    outputfile << ", " << finalComponents->at(b).componentOutputs.at(0).Name << ", " << finalComponents->at(b).componentOutputs.at(1).Name;
                    outputfile << ", " << finalComponents->at(b).componentOutputs.at(2).Name << ");" << std::endl;
                    comp++;
                }
                
                // MUX2x1
                else if(finalComponents->at(b).compName.compare("MUX2x1") == 0) {
                    outputfile << finalComponents->at(b).compName << " #(.DATAWIDTH(" << finalComponents->at(b).size << ")) ";
                    outputfile << "mux_" << mux << "(" << finalComponents->at(b).componentInputs.at(0).Name << ", " << finalComponents->at(b).componentInputs.at(1).Name;
                    outputfile << ", " << finalComponents->at(b).componentInputs.at(2).Name << ", " << finalComponents->at(b).componentOutputs.at(0).Name;
                    outputfile << ");" << std::endl;
                    mux++;
                }
                
                // SHR
                else if(finalComponents->at(b).compName.compare("SHR") == 0) {
                    outputfile << finalComponents->at(b).compName << " #(.DATAWIDTH(" << finalComponents->at(b).size << ")) ";
                    outputfile << "shr_" << shr << "(" << finalComponents->at(b).componentInputs.at(0).Name << ", " << finalComponents->at(b).componentInputs.at(1).Name;
                    outputfile << ", " << finalComponents->at(b).componentOutputs.at(0).Name << ");" << std::endl;
                    shr++;
                }
                
                // SHL
                else if(finalComponents->at(b).compName.compare("SHL") == 0) {
                    outputfile << finalComponents->at(b).compName << " #(.DATAWIDTH(" << finalComponents->at(b).size << ")) ";
                    outputfile << "shl_" << shl << "(" << finalComponents->at(b).componentInputs.at(0).Name << ", " << finalComponents->at(b).componentInputs.at(1).Name;
                    outputfile << ", " << finalComponents->at(b).componentOutputs.at(0).Name << ");" << std::endl;
                    shl++;
                }
                
                // DIV
                else if(finalComponents->at(b).compName.compare("DIV") == 0) {
                    outputfile << finalComponents->at(b).compName << " #(.DATAWIDTH(" << finalComponents->at(b).size << ")) ";
                    outputfile << "div_" << div << "(" << finalComponents->at(b).componentInputs.at(0).Name << ", " << finalComponents->at(b).componentInputs.at(1).Name;
                    outputfile << ", " << finalComponents->at(b).componentOutputs.at(0).Name << ");" << std::endl;
                    div++;
                }
                
                // MOD
                else if(finalComponents->at(b).compName.compare("MOD") == 0) {
                    outputfile << finalComponents->at(b).compName << " #(.DATAWIDTH(" << finalComponents->at(b).size << ")) ";
                    outputfile << "mod_" << mod << "(" << finalComponents->at(b).componentInputs.at(0).Name << ", " << finalComponents->at(b).componentInputs.at(1).Name;
                    outputfile << ", " << finalComponents->at(b).componentOutputs.at(0).Name << ");" << std::endl;
                    mod++;
                }
                
                // INC
                else if(finalComponents->at(b).compName.compare("INC") == 0) {
                    outputfile << finalComponents->at(b).compName << " #(.DATAWIDTH(" << finalComponents->at(b).size << ")) ";
                    outputfile << "inc_" << inc << "(" << finalComponents->at(b).componentInputs.at(0).Name << ", " << finalComponents->at(b).componentInputs.at(1).Name;
                    outputfile << ", " << finalComponents->at(b).componentOutputs.at(0).Name << ");" << std::endl;
                    inc++;
                }
                
                // DEC
                else if(finalComponents->at(b).compName.compare("DEC") == 0) {
                    outputfile << finalComponents->at(b).compName << " #(.DATAWIDTH(" << finalComponents->at(b).size << ")) ";
                    outputfile << "dec_" << dec << "(" << finalComponents->at(b).componentInputs.at(0).Name << ", " << finalComponents->at(b).componentInputs.at(1).Name;
                    outputfile << ", " << finalComponents->at(b).componentOutputs.at(0).Name << ");" << std::endl;
                    dec++;
                }
                
                // REG
                else if(finalComponents->at(b).compName.compare("REG") == 0) {
                    outputfile << finalComponents->at(b).compName << " #(.DATAWIDTH(" << finalComponents->at(b).size << ")) ";
                    outputfile << "reg_" << reg << "(" << finalComponents->at(b).componentInputs.at(2).Name << ", " << finalComponents->at(b).componentInputs.at(0).Name;
                    outputfile << ", " << finalComponents->at(b).componentInputs.at(1).Name << ", ";
                    outputfile << finalComponents->at(b).componentOutputs.at(0).Name << ");" << std::endl;
                    reg++;
                }
                
                else if(finalComponents->at(b).compName.compare("assign") == 0) {
                    outputfile << finalComponents->at(b).compName << " " << finalComponents->at(b).componentOutputs.at(0).Name << " = ";
                    outputfile << "{" << finalComponents->at(b).componentInputs.at(0).Name << ", " << finalComponents->at(b).componentInputs.at(1).Name;
                    outputfile << "};" << std::endl;
                }
                
                
            }
        }
        
        else if (a == 6) {
            outputfile << std::endl << "endmodule";
        }
        
        
    }
    
    
    
    outputfile.close();
    
}
    

int checkSize(component *comp2check, component *x, connectionType *o) {
    int a = 0;
    int b = 0;
    int sizeDiff = 0;
    std::string tempSize;
    std::string tempName;
    connectionType d;
    
    std::stringstream holder(comp2check->componentInputs.at(0).Size);
    std::stringstream holder2(comp2check->componentInputs.at(1).Size);
    holder >> a;
    holder2 >> b;
    
    if(a > b) {
        sizeDiff = a - b;
        tempSize = std::to_string(sizeDiff);
        d.Size.assign(tempSize);
        tempSize.append("'b0");
        d.Name.assign(tempSize);
        d.input = false;
        d.output = false;
        d.regist = false;
        d.wire = true;
        
        
        tempName.assign(comp2check->componentInputs.at(1).Name);
        tempName.append("_temp");
        o->Name.assign(tempName);
        o->Size.assign(comp2check->componentInputs.at(0).Size);
        o->input = false;
        o->output = false;
        o->regist = false;
        o->wire = true;
        
        x->compName.assign("assign");
        x->componentInputs.push_back(d);
        x->componentInputs.push_back(comp2check->componentInputs.at(1));
        return 1;
    }
        
    else if(a < b) {
        sizeDiff = b - a;
        tempSize = std::to_string(sizeDiff);
        d.Size.assign(tempSize);
        tempSize.append("'b0");
        d.Name.assign(tempSize);
        d.input = false;
        d.output = false;
        d.regist = false;
        d.wire = true;
        
        
        tempName.assign(comp2check->componentInputs.at(0).Name);
        tempName.append("_temp");
        o->Name.assign(tempName);
        o->Size.assign(comp2check->componentInputs.at(1).Size);
        o->input = false;
        o->output = false;
        o->regist = false;
        o->wire = true;
        
        x->compName.assign("assign");
        x->componentInputs.push_back(d);
        x->componentInputs.push_back(comp2check->componentInputs.at(0));
        return 1;
    }
    
    
    
    
    
    
    
    return 0;
}


int signExtendHelper(connectionType co1, connectionType co2) {
    int x = 0;
    int y = 0;
    int diff = 0;
    std::string bitInput;
    
    std::stringstream holder(co1.Size);
    std::stringstream holder2(co2.Size);
    
    holder >> x;
    holder2 >> y;
    
    if(x > y) {
        diff = x - y;
        return diff;
    }
    
    else if(y > x) {
        diff = y - x;
        return diff;
    }
    
    
    else{
        return 0;
    }
}

