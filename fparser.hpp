//
//  parser.hpp
//  Datapath Generator
//  MODIFIED BY FRANK P. las update 11/16/2020 @ 8:35PM COMPILES FINE
//  Created by Brady Thomas on 10/28/20.
//

#ifndef parser_hpp
#define parser_hpp
#include <fstream>
#include <iostream>
#include <vector>
#include "dpgen.hpp"
#include <string>
#include <stdio.h>

class connectionType
{
public:
    std::string Size; //8 //16
    std::string Name; //a //b //c
    bool output;
    bool wire;
    bool input;
    /*connectionType(std::string size1, std::string name1, bool o1, bool w1, bool i1){
        Size = size1;
        Name = name1;
        output = o1;
        wire = w1;
        input = i1;
    } */
    std::string getSize()
    {
        return this->Size;
    }
};

class component
{
public:
    double latency;
    std::string compName;//component name i.e ADDER
    std::string size; //8 16 32
    std::vector <connectionType> componentInputs;
    std::vector <connectionType> componentOutputs;
};

//FRANK'S CODE DO NOT DELETE
class INPUTCount
{
public:
    int inputcount( int INPUTcount);
};
class WIRECount
{
public:
    int wirecount( int WIRECount);

};
class OUTPUTCount
{
public:
    int outputcount( int OUTPUTcount);
};
class componentADDCount
{
public:
    int addercount( int Addercount);
};
class componentSUBCount
{
public:
    int subcount( int Subrcount);
};
class componentMULCount
{
public:
    int mulcount( int Mulcount);
};
class componentCOMPCount
{
public:
    int compcount( int Comprcount);
};
class componentMUXCount
{
public:
    int muxcount( int Muxprcount);
};
class componentSHRCount
{
public:
    int shrcount( int shrcount);
};
class componentSHLCount
{
public:
    int shlcount( int shlcount);
};
class componentDIVCount
{
public:
    int divcount( int Divcount);
};
class componentMODCount
{
public:
    int modcount( int Modcount);
};
class componentINCCount
{
public:
    int inccount( int Inccount);
};
class componentDECCount
{
public:
    int deccount( int Deccount);
};
class componentREGCount
{
public:
    int regcount( int regcount);
};

class booleanbit
{
public:
    int adder1btrue     (int  adder1btrue);
    int adder2btrue     (int  adder2btrue);
    int adder8btrue     (int  adder8btrue);
    int adder16btrue    (int  adder16btrue);
    int adder32btrue    (int  adder32btrue);
    int adder64btrue    (int  adder64btrue);

    int sub1true        (int  sub1btrue);
    int sub2true        (int  sub2btrue);
    int sub8true        (int  sub8btrue);
    int sub16true       (int  sub16btrue);
    int sub32true       (int  sub32btrue);
    int sub64true       (int  sub64btrue);

    int mul1true        (int  mul1btrue);
    int mul2true        (int  mul2btrue);
    int mul8true        (int  mul8btrue);
    int mul16true       (int  mul16btrue);
    int mul32rue        (int  mul32btrue);
    int mul64true       (int  mul64btrue);

    int comp1true        (int  comp1btrue);
    int comp2true        (int  comp2btrue);
    int comp8true        (int  comp8btrue);
    int comp16true       (int  comp16btrue);
    int comp32true       (int  comp32btrue);
    int comp64true       (int  comp64btrue);

    int mux1true         (int  mux1btrue);
    int mux2true         (int  mux2btrue);
    int mux8true         (int  mux8btrue);
    int mux16true        (int  mux16btrue);
    int mux32true        (int  mux32btrue);
    int mux64true        (int  mux64btrue);

    int shr1true         (int  shr1btrue);
    int shr2true         (int  shr2btrue);
    int shr8true         (int  shr8btrue);
    int shr16true        (int  shr16btrue);
    int shr32true        (int  shr32btrue);
    int shr64true        (int  shr64btrue);

    int shl1true         (int  shlb1true);
    int shl2true         (int  shlb2true);
    int shl8true         (int  shlb8true);
    int shl16true        (int  shlb16true);
    int shlt32rue        (int  shlb32true);
    int shlt64rue        (int  shlb64true);

    int div1true        (int  div1btrue);
    int div2true        (int  div2btrue);
    int div8true        (int  div8btrue);
    int div16true       (int  div16btrue);
    int div32true       (int  div32btrue);
    int div64true       (int  div64btrue);

    int mod1true         (int  mod1btrue);
    int mod2true         (int  mod2btrue);
    int mod8true         (int  mod8btrue);
    int mod16true        (int  mod16btrue);
    int mod32true        (int  mod32btrue);
    int mod64true        (int  mod64btrue);

    int inc1true         (int  inc1btrue);
    int inc2true         (int  inc2btrue);
    int inc8true         (int  inc8btrue);
    int inc16true        (int  inc16btrue);
    int inc32true        (int  inc32btrue);
    int inc64true        (int  inc64btrue);

    int dec1true         (int  dec1btrue);
    int dec2true         (int  dec2btrue);
    int dec8true         (int  dec8btrue);
    int dec16true        (int  dec16btrue);
    int dec32true        (int  dec32true);
    int dec64true        (int  dec64true);

    int reg1true         (int  reg1btrue);
    int reg2true         (int  reg2btrue);
    int reg8true         (int  reg8btrue);
    int reg16true        (int  reg16btrue);
    int reg32true        (int  reg32btrue);
    int reg64true        (int  reg64btrue);

    int connectionADDtrue   (int ADDtrue);
    int connectionREGtrue   (int REGtrue);
    int connectionSUBtrue   (int SUBtrue);
    int connectionMODtrue   (int MODtrue);
    int connectionMULtrue   (int MULtrue);
    int connectionCOMPtrue  (int COMPtrue);
    int connectionMUXtrue   (int MUXtrue);
    int connectionSHRtrue   (int SHRtrue);
    int connectionSHLtrue   (int SHLtrue);
    int connectionDIVtrue   (int DIVtrue);
    int connectionINCtrue   (int INCtrue);
    int connectionDECtrue   (int DECtrue);

};

//FRANKS CODE DO NOT DELETE


void parseFile(std::ifstream& inFile);
void evaluateString(std::string& line, std::vector<connectionType> *verilogConnections, std::vector<component> *verilogComponents);
void writeVerilog(std::string& line);




#endif /* parser_hpp */
