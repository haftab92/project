//
//  parser.hpp
//  Datapath Generator
//
//  Created by Brady Thomas on 10/28/20.
//

#ifndef parser_hpp
#define parser_hpp
#include <fstream>
#include <iostream>
#include <vector>
#include "dpgen.hpp"
#include <string>


#include <stdio.h>



class connectionType {
public:
    std::string Size;
    std::string Name;
    bool output;
    bool wire;
    bool input;
    bool regist;
    int sign = 0;
    /*connectionType(std::string size1, std::string name1, bool o1, bool w1, bool i1){
        Size = size1;
        Name = name1;
        output = o1;
        wire = w1;
        input = i1;
    } */
    std::string getSize() {
        return this->Size;
    }
    int getSign(){
        return this->sign;
    }
};

class component{
public:
    double latency;
    int sign;
    std::string compName;
    std::string size;
    std::vector <connectionType> componentInputs;
    std::vector <connectionType> componentOutputs;
};

class parser{
public:
   int parseFile(const char *filename);
};
   
int evaluateString(std::string& line, std::vector<connectionType> *verilogConnections, std::vector<component> *verilogComponents);
void writeVerilog(std::vector<connectionType> *verilogConnections, std::vector<component> *verilogComponents, int linenum);
int checkSize(component *comp2check, component *comp2modify, connectionType *newWire);
int signExtendHelper(connectionType co1, connectionType co2);



#endif /* parser_hpp */
