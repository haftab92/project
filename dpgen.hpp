//
//  dpgen.hpp
//  Datapath Generator
//
//  Created by Brady Thomas on 10/28/20.
//

#ifndef dpgen_hpp
#define dpgen_hpp

#include <stdio.h>
#include "parser.hpp"
#include <string>
#include <vector>
#include <iostream>
#include <fstream>





double setLatencyValues(std::string& componentSize, std::string& componentName);
double calccritpath();


#endif /* dpgen_hpp */
