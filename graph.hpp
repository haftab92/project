//
//  graph.hpp
//  Datapath Generator
//
//  Created by Brady Thomas on 10/28/20.
//  MODIFIED BY FRANK P. las update 11/16/2020 @ 8:35PM COMPILES FINE
//

#ifndef graph_hpp
#define graph_hpp
#include <stdio.h>
#include <vector>
#include <list>
#include <limits.h>
#include <stack>
#include "datatypes.hpp"
#include "parser.hpp"
#define NINF INT_MIN
#include<bits/stdc++.h>
using namespace std;

//STRUCS FOR DATATYPES
struct componentAdderWeight
{
    double adder1bweight ;
    double adder2bweight ;
    double adder8bweight ;
    double adder16bweight;
    double adder32weight ;
    double adder64bweight;
};

struct componentSUBWeight
{
    double SUB1bweight;
    double SUB2bweight;
    double SUB8bweight;
    double SUB16bweight;
    double SUB32weight;
    double SUB64bweight;
}
;
struct componentMULWeight
{
    double MUL1bweight;
    double MUL2bweight;
    double MUL8bweight;
    double MUL16bweight;
    double MUL32weight;
    double MUL64bweight;
};
struct componentCOMPWeight
{
    double COMP1bweight;
    double COMP2bweight;
    double COMP8bweight;
    double COMP16bweight;
    double COMP32weight;
    double COMP64bweight;
};
struct componentMUX2x1Weight
{
    double MUX2x11bweight;
    double MUX2x12bweight;
    double MUX2x18bweight;
    double MUX2x16bweight;
    double MUX2x132weight;
    double MUX2x164bweight;
}
;
struct componentSHRWeight
{
    double SHR1bweight;
    double SHR2bweight;
    double SHR8bweight;
    double SHR16bweight;
    double SHR32weight;
    double SHR64bweight;
};
struct componentSHLWeight
{
    double SHL1bweight;
    double SHL2bweight;
    double SHL8bweight;
    double SHL16bweight;
    double SHL32weight;
    double SHL64bweight;
};
struct componentDIVWeight
{
    double DIV1bweight;
    double DIV2bweight;
    double DIV8bweight;
    double DIV16bweight;
    double DIV32weight;
    double DIV64bweight;
};
struct componentMODWeight
{
    double MOD1bweight;
    double MOD2bweight;
    double MOD8bweight;
    double MOD16bweight;
    double MOD32weight;
    double MOD64bweight;
};
struct componentINCWeight
{
    double INC1bweight;
    double INC2bweight;
    double INC8bweight;
    double INC16bweight;
    double INC32weight;
    double INC64bweight;
};
struct componentDECWeight
{
    double DEC1bweight;
    double DEC2bweight;
    double DEC8bweight;
    double DEC16bweight;
    double DEC32weight;
    double DEC64bweight;
};
struct componentREGWeight
{
    double REG1bweight;
    double REG2bweight;
    double REG8bweight;
    double REG16bweight;
    double REG32weight;
    double REG64bweight;
};

//OTHER CLASS STUFF FOR GETTERS IN MAIN
class totals
{
public:
    double addertotals (double addertotals1);
    double regstotals  (double regtotals1);
    double subtotals   (double subtotals1);
    double multotals   (double multotals1);
    double comptotals  (double comptotals1);
    double muxtotals   (double muxtotals1);
    double shrtotals   (double shrtotals1);
    double shltotals   (double shltotals1);
    double divtotals   (double divtotals1);
    double modtotals   (double modtotals1);
    double inctotals   (double inctotals1);
    double dectotals   (double dectotals1);
};

//FINAL CRITICAL PATHS VALS HERE FOR ACTUAL CONNECTIONS
class finalvalues
{
public:
    double addertotalsF (double addertotals1F);
    double regstotalsF  (double regtotals1F);
    double subtotalsF   (double subtotals1F);
    double multotalsF   (double multotals1F);
    double comptotalsF  (double comptotals1F);
    double muxtotalsF   (double muxtotals1F);
    double shrtotalsF   (double shrtotals1F);
    double shltotalsF   (double shltotals1F);
    double divtotalsF   (double divtotals1F);
    double modtotalsF   (double modtotals1F);
    double inctotalsF   (double inctotals1F);
    double dectotalsF   (double dectotals1F);

};

totals  t;
finalvalues f;

//ADDER WEIGHTS
double calculatingPath(double a, int numComCir)
{
    double totalweight;
    totalweight = a * numComCir;
    t.addertotals(totalweight);
    return totalweight;
}


//REG WEIGHTs
double calculatingPath1(double a, int numComCir)
{
    double totalweight;
    totalweight = a * numComCir;
    t.regstotals(totalweight);
    return totalweight;
}

//SUBWEIGHT
double calculatingPath2(double a, int numComCir)
{
    double totalweight;
    totalweight = a * numComCir;
    t.subtotals(totalweight);
    return totalweight;
}

//MULWEIGHTs
double calculatingPath3(double a, int numComCir)
{
    double totalweight;
    totalweight = a * numComCir;
    t.multotals(totalweight);
    return totalweight;
}

//COMPWEIGHTs
double calculatingPath4(double a, int numComCir)
{
    double totalweight;
    totalweight = a * numComCir;
    t.comptotals(totalweight);
    return totalweight;
}

//MUX WEIGHTS
double calculatingPath5(double a, int numComCir)
{
    double totalweight;
    totalweight = a * numComCir;
    t.muxtotals(totalweight);
    return totalweight;
}

//SHR WEIGHTS
double calculatingPath6(double a, int numComCir)
{
    double totalweight;
    totalweight = a * numComCir;
    t.shrtotals(totalweight);
    return totalweight;
}

//SHL WEIGHTS
double calculatingPath7(double a, int numComCir)
{
    double totalweight;
    totalweight = a * numComCir;
    t.shltotals(totalweight);
    return totalweight;
}

//DIV WEIGHTS
double calculatingPath8(double a, int numComCir)
{
    double totalweight;
    totalweight = a * numComCir;
    t.divtotals(totalweight);
    return totalweight;
}

//MODWEIGHTS
double calculatingPath9(double a, int numComCir)
{
    double totalweight;
    totalweight = a * numComCir;
    t.modtotals(totalweight);
    return totalweight;
}

//INCWEIGHTS
double calculatingPath10(double a, int numComCir)
{
    double totalweight;
    totalweight = a * numComCir;
    t.inctotals(totalweight);
    return totalweight;
}

//DECWEIGHTS
double calculatingPath11(double a, int numComCir)
{
    double totalweight;
    totalweight = a * numComCir;
    t.dectotals(totalweight);
    return totalweight;
}

//adder
double sortingTOTALSCalc(double a)
{
    double fval;
    fval = a;
    f.addertotalsF(fval);
    return fval;
}

//reg
double sortingTOTALSCalc1(double a)
{
    double fval;
    fval = a;
    f.regstotalsF(fval);
    return fval;
}

//sub
double sortingTOTALSCalc2(double a)
{
    double fval;
    fval = a;
    f.subtotalsF(fval);
    return fval;
}

//mul
double sortingTOTALSCalc3(double a)
{
    double fval;
    fval = a;
    f.subtotalsF(fval);
    return fval;
}

//comp
double sortingTOTALSCalc4(double a)
{
    double fval;
    fval = a;
    f.comptotalsF(fval);
    return fval;
}

//mux
double sortingTOTALSCalc5(double a)
{
    double fval;
    fval = a;
    f.muxtotalsF(fval);
    return fval;
}

//shr
double sortingTOTALSCalc6(double a)
{
    double fval;
    fval = a;
    f.shrtotalsF(fval);
    return fval;
}

//shl
double sortingTOTALSCalc7(double a)
{
    double fval;
    fval = a;
    f.shltotalsF(fval);
    return fval;
}

//div
double sortingTOTALSCalc8(double a)
{
    double fval;
    fval = a;
    f.divtotalsF(fval);
    return fval;
}

//mod
double sortingTOTALSCalc9(double a)
{
    double fval;
    fval = a;
    f.modtotalsF(fval);
    return fval;
}

//inc
double sortingTOTALSCalc10(double a)
{
    double fval;
    fval = a;
    f.inctotalsF(fval);
    return fval;
}

//dec
double sortingTOTALSCalc11(double a)
{
    double fval;
    fval = a;
    f.dectotalsF(fval);
    return fval;
}

//FINAL CRIT PATH VALUES HERE
void FINALCCalculated(double ad, double rg, double sb, double mul, double comp, double mx, double sr, double sl, double dv, double md, double incr, double dcm)
{
    double criticalPath;
    criticalPath = ad + rg + sb + mul + comp + mx + sr + sl + dv + md + incr + dcm;
    cout<<"\n";
    cout<<"\n";
    cout<<"CRITICAL PATH in ns :::";
    cout<<criticalPath;
}

//FUNCTION FOR IF MODULES EXITS, NAME, and HOW MANY TIMES THAT MODULE(i.e ADDER...) exists and link up with weight value
void criticalPathCalcComps(int a, string name, double weight, int numberofComps)
{

//adder
    if(a == 1 && name.compare("adder1") == 0)
    {
        weight;
        numberofComps;
        calculatingPath(weight, numberofComps);
    }
    else if(a == 1 && name.compare("adder2") == 0)
    {
        weight;
        numberofComps;
        calculatingPath(weight, numberofComps);
    }
    else if(a == 1 && name.compare("adder8") == 0)
    {
        weight;
        numberofComps;
        calculatingPath(weight, numberofComps);
    }
    else if(a == 1 && name.compare("adder16") == 0)
    {
        weight;
        numberofComps;
        calculatingPath(weight, numberofComps);
    }
    else if(a == 1 && name.compare("adder32") == 0)
    {
        weight;
        numberofComps;
        calculatingPath(weight, numberofComps);
    }
    else if(a == 1 && name.compare("adder64") == 0)
    {
        weight;
        numberofComps;
        calculatingPath(weight, numberofComps);
    }

//regs
    if(a == 1 && name.compare("reg1") == 0)
    {
        weight;
        numberofComps;
        calculatingPath1(weight, numberofComps);
    }
    else if(a == 1 && name.compare("reg2") == 0)
    {
        weight;
        numberofComps;
        calculatingPath1(weight, numberofComps);
    }
    else if(a == 1 && name.compare("reg8") == 0)
    {
        weight;
        numberofComps;
        calculatingPath1(weight, numberofComps);
    }
    else if(a == 1 && name.compare("reg16") == 0)
    {
        weight;
        numberofComps;
        calculatingPath1(weight, numberofComps);
    }
    else if(a == 1 && name.compare("reg32") == 0)
    {
        weight;
        numberofComps;
        calculatingPath1(weight, numberofComps);
    }
    else if(a == 1 && name.compare("reg64") == 0)
    {
        weight;
        numberofComps;
        calculatingPath1(weight, numberofComps);
    }
//subs

    if(a == 1 && name.compare("sub1") == 0)
    {
        weight;
        numberofComps;
        calculatingPath2(weight, numberofComps);
    }
    else if(a == 1 && name.compare("sub2") == 0)
    {
        weight;
        numberofComps;
        calculatingPath2(weight, numberofComps);
    }
    else if(a == 1 && name.compare("sub8") == 0)
    {
        weight;
        numberofComps;
        calculatingPath2(weight, numberofComps);
    }
    else if(a == 1 && name.compare("sub16") == 0)
    {
        weight;
        numberofComps;
        calculatingPath2(weight, numberofComps);
    }
    else if(a == 1 && name.compare("sub32") == 0)
    {
        weight;
        numberofComps;
        calculatingPath2(weight, numberofComps);
    }
    else if(a == 1 && name.compare("sub64") == 0)
    {
        weight;
        numberofComps;
        calculatingPath2(weight, numberofComps);
    }

//MULS
    if(a == 1 && name.compare("mul1") == 0)
    {
        weight;
        numberofComps;
        calculatingPath3(weight, numberofComps);
    }
    else if(a == 1 && name.compare("mul2") == 0)
    {
        weight;
        numberofComps;
        calculatingPath3(weight, numberofComps);
    }
    else if(a == 1 && name.compare("mul8") == 0)
    {
        weight;
        numberofComps;
        calculatingPath3(weight, numberofComps);
    }
    else if(a == 1 && name.compare("mul16") == 0)
    {
        weight;
        numberofComps;
        calculatingPath3(weight, numberofComps);
    }
    else if(a == 1 && name.compare("mul32") == 0)
    {
        weight;
        numberofComps;
        calculatingPath3(weight, numberofComps);
    }
    else if(a == 1 && name.compare("mul64") == 0)
    {
        weight;
        numberofComps;
        calculatingPath3(weight, numberofComps);
    }

//COMPS
    if(a == 1 && name.compare("comp1") == 0)
    {
        weight;
        numberofComps;
        calculatingPath4(weight, numberofComps);
    }
    else if(a == 1 && name.compare("comp2") == 0)
    {
        weight;
        numberofComps;
        calculatingPath4(weight, numberofComps);
    }
    else if(a == 1 && name.compare("comp8") == 0)
    {
        weight;
        numberofComps;
        calculatingPath4(weight, numberofComps);
    }
    else if(a == 1 && name.compare("comp16") == 0)
    {
        weight;
        numberofComps;
        calculatingPath4(weight, numberofComps);
    }
    else if(a == 1 && name.compare("comp32") == 0)
    {
        weight;
        numberofComps;
        calculatingPath4(weight, numberofComps);
    }
    else if(a == 1 && name.compare("comp64") == 0)
    {
        weight;
        numberofComps;
        calculatingPath4(weight, numberofComps);
    }

//MUXes
    if(a == 1 && name.compare("mux1") == 0)
    {
        weight;
        numberofComps;
        calculatingPath5(weight, numberofComps);
    }
    else if(a == 1 && name.compare("mux2") == 0)
    {
        weight;
        numberofComps;
        calculatingPath5(weight, numberofComps);
    }
    else if(a == 1 && name.compare("mux8") == 0)
    {
        weight;
        numberofComps;
        calculatingPath5(weight, numberofComps);
    }
    else if(a == 1 && name.compare("mux16") == 0)
    {
        weight;
        numberofComps;
        calculatingPath5(weight, numberofComps);
    }
    else if(a == 1 && name.compare("mux32") == 0)
    {
        weight;
        numberofComps;
        calculatingPath5(weight, numberofComps);
    }
    else if(a == 1 && name.compare("mux64") == 0)
    {
        weight;
        numberofComps;
        calculatingPath5(weight, numberofComps);
    }

//SHR
    if(a == 1 && name.compare("shr1") == 0)
    {
        weight;
        numberofComps;
        calculatingPath6(weight, numberofComps);
    }
    else if(a == 1 && name.compare("shr2") == 0)
    {
        weight;
        numberofComps;
        calculatingPath6(weight, numberofComps);
    }
    else if(a == 1 && name.compare("shr8") == 0)
    {
        weight;
        numberofComps;
        calculatingPath6(weight, numberofComps);
    }
    else if(a == 1 && name.compare("shr16") == 0)
    {
        weight;
        numberofComps;
        calculatingPath6(weight, numberofComps);
    }
    else if(a == 1 && name.compare("shr32") == 0)
    {
        weight;
        numberofComps;
        calculatingPath6(weight, numberofComps);
    }
    else if(a == 1 && name.compare("shr64") == 0)
    {
        weight;
        numberofComps;
        calculatingPath6(weight, numberofComps);
    }

//SHL
    if(a == 1 && name.compare("shl1") == 0)
    {
        weight;
        numberofComps;
        calculatingPath7(weight, numberofComps);
    }
    else if(a == 1 && name.compare("shl2") == 0)
    {
        weight;
        numberofComps;
        calculatingPath7(weight, numberofComps);
    }
    else if(a == 1 && name.compare("shl8") == 0)
    {
        weight;
        numberofComps;
        calculatingPath7(weight, numberofComps);
    }
    else if(a == 1 && name.compare("shl16") == 0)
    {
        weight;
        numberofComps;
        calculatingPath7(weight, numberofComps);
    }
    else if(a == 1 && name.compare("shl32") == 0)
    {
        weight;
        numberofComps;
        calculatingPath7(weight, numberofComps);
    }
    else if(a == 1 && name.compare("shl64") == 0)
    {
        weight;
        numberofComps;
        calculatingPath7(weight, numberofComps);
    }

//DIV
    if(a == 1 && name.compare("div1") == 0)
    {
        weight;
        numberofComps;
        calculatingPath8(weight, numberofComps);
    }
    else if(a == 1 && name.compare("div2") == 0)
    {
        weight;
        numberofComps;
        calculatingPath8(weight, numberofComps);
    }
    else if(a == 1 && name.compare("div8") == 0)
    {
        weight;
        numberofComps;
        calculatingPath8(weight, numberofComps);
    }
    else if(a == 1 && name.compare("div16") == 0)
    {
        weight;
        numberofComps;
        calculatingPath8(weight, numberofComps);
    }
    else if(a == 1 && name.compare("div32") == 0)
    {
        weight;
        numberofComps;
        calculatingPath8(weight, numberofComps);
    }
    else if(a == 1 && name.compare("div64") == 0)
    {
        weight;
        numberofComps;
        calculatingPath8(weight, numberofComps);
    }

//MOD
    if(a == 1 && name.compare("mod1") == 0)
    {
        weight;
        numberofComps;
        calculatingPath9(weight, numberofComps);
    }
    else if(a == 1 && name.compare("mod2") == 0)
    {
        weight;
        numberofComps;
        calculatingPath9(weight, numberofComps);
    }
    else if(a == 1 && name.compare("mod8") == 0)
    {
        weight;
        numberofComps;
        calculatingPath9(weight, numberofComps);
    }
    else if(a == 1 && name.compare("mod16") == 0)
    {
        weight;
        numberofComps;
        calculatingPath9(weight, numberofComps);
    }
    else if(a == 1 && name.compare("mod32") == 0)
    {
        weight;
        numberofComps;
        calculatingPath9(weight, numberofComps);
    }
    else if(a == 1 && name.compare("mod64") == 0)
    {
        weight;
        numberofComps;
        calculatingPath9(weight, numberofComps);
    }

//INC

    if(a == 1 && name.compare("inc1") == 0)
    {
        weight;
        numberofComps;
        calculatingPath10(weight, numberofComps);
    }
    else if(a == 1 && name.compare("inc2") == 0)
    {
        weight;
        numberofComps;
        calculatingPath10(weight, numberofComps);
    }
    else if(a == 1 && name.compare("inc8") == 0)
    {
        weight;
        numberofComps;
        calculatingPath10(weight, numberofComps);
    }
    else if(a == 1 && name.compare("inc16") == 0)
    {
        weight;
        numberofComps;
        calculatingPath10(weight, numberofComps);
    }
    else if(a == 1 && name.compare("inc32") == 0)
    {
        weight;
        numberofComps;
        calculatingPath(weight, numberofComps);
    }
    else if(a == 1 && name.compare("inc64") == 0)
    {
        weight;
        numberofComps;
        calculatingPath10(weight, numberofComps);
    }

//DEC

    if(a == 1 && name.compare("dec1") == 0)
    {
        weight;
        numberofComps;
        calculatingPath11(weight, numberofComps);
    }
    else if(a == 1 && name.compare("dec2") == 0)
    {
        weight;
        numberofComps;
        calculatingPath11(weight, numberofComps);
    }
    else if(a == 1 && name.compare("dec8") == 0)
    {
        weight;
        numberofComps;
        calculatingPath11(weight, numberofComps);
    }
    else if(a == 1 && name.compare("dec16") == 0)
    {
        weight;
        numberofComps;
        calculatingPath11(weight, numberofComps);
    }
    else if(a == 1 && name.compare("dec32") == 0)
    {
        weight;
        numberofComps;
        calculatingPath11(weight, numberofComps);
    }
    else if(a == 1 && name.compare("dec64") == 0)
    {
        weight;
        numberofComps;
        calculatingPath11(weight, numberofComps);
    }



    ;
}




#endif  /*graph_hpp*/
