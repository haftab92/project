//
//  datatypes.hpp
//  Datapath Generator
//
//  Created by Brady Thomas on 10/28/20.
//

#ifndef datatypes_hpp
#define datatypes_hpp

#include <stdio.h>

class dataType {
public:
    // Register latencies
    double reg1b = 2.616;
    double reg2b = 2.644;
    double reg8b = 2.879;
    double reg16b = 3.061;
    double reg32b = 3.062;
    double reg64b = 3.966;
    // Adder latencies
    double add1b = 2.704;
    double add2b = 3.713;
    double add8b = 4.924;
    double add16b = 5.638;
    double add32b = 7.270;
    double add64b = 9.566;
    // Subtractor latencies
    double sub1b = 3.024;
    double sub2b = 3.412;
    double sub8b = 4.890;
    double sub16b = 5.569;
    double sub32b = 7.253;
    double sub64b = 9.566;
    // Multiplier latencies
    double mult1b = 2.438;
    double mult2b = 3.651;
    double mult8b = 7.543;
    double mult16b = 7.811;
    double mult32b = 12.395;
    double mult64b = 15.354;
    // Comparator latencies
    double comp1b = 3.031;
    double comp2b = 3.934;
    double comp8b = 5.949;
    double comp16b = 6.256;
    double comp32b = 7.264;
    double comp64b = 8.416;
    // MUX latencies
    double mux1b = 4.083;
    double mux2b = 4.115;
    double mux8b = 4.815;
    double mux16b = 5.623;
    double mux32b = 8.079;
    double mux64b = 8.766;
    // SHR latencies
    double shr1b = 3.644;
    double shr2b = 4.007;
    double shr8b = 5.178;
    double shr16b = 6.460;
    double shr32b = 8.819;
    double shr64b = 11.095;
    // SHL latencies
    double shl1b = 3.614;
    double shl2b = 3.980;
    double shl8b = 5.152;
    double shl16b = 6.549;
    double shl32b = 8.565;
    double shl64b = 11.220;
    // Divider latencies
    double div1b = 0.619;
    double div2b = 2.144;
    double div8b = 15.439;
    double div16b = 33.093;
    double div32b = 86.312;
    double div64b = 243.233;
    // MOD latencies
    double mod1b = 0.758;
    double mod2b = 2.149;
    double mod8b = 16.078;
    double mod16b = 35.563;
    double mod32b = 88.142;
    double mod64b = 250.583;
    // Incrementer latencies
    double inc1b = 1.792;
    double inc2b = 2.218;
    double inc8b = 3.111;
    double inc16b = 3.471;
    double inc32b = 4.347;
    double inc64b = 6.200;
    // Decrementer latencies
    double dec1b = 1.792;
    double dec2b = 2.218;
    double dec8b = 3.108;
    double dec16b = 3.701;
    double dec32b = 4.685;
    double dec64b = 6.503;
};

#endif /* datatypes_hpp */
