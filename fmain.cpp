//
//  main.cpp
//  Datapath Generator
//
//  Created by Brady Thomas on 10/28/20.
//  MODIFIED BY FRANK P. las update 11/16/2020 @ 8:35PM COMPILES FINE
//

#include <iostream>
#include <fstream>
#include "graph.hpp"
#include "dpgen.hpp"
#include "datatypes.hpp"
#include <vector>
#include "parser.hpp"
#include <string>

using namespace std;

//THIS IS A MESSY CLASS BASICALLY A BUNCH OF STUFF HAPPENS DOWN THERE
class COUNTVAL
{
public:
    int input;
    int wire;
    int output;
    int Adder;
    int Sub;
    int Mul;
    int Comp;
    int Mux;
    int Shr;
    int Shl;
    int Mod;
    int Div;
    int Inc;
    int Dec;
    int Reg;
    int bitwire;
    int connADD, connreg, connsub, connmul, conComp, conmux, conshr, conshl, condiv, conmod, coninc, condec;
    double connADD1, connreg1, connsub1, connmul1, conComp1, conmux1, conshr1, conshl1, condiv1, conmod1, coninc1, condec1;
    double finaADD, finREG, finSUB, finCMP, finMUL, finMUX, finSHR, finSHL, finDIV, finMOD, finINC, finDEC;
    int addtrue1b, sub1true, reg1true,  mul1true,  comp1true,  mux1true,  shr1true,  shl1true,  div1true,  inc1true,  dec1true, mod1true ;
    int addtrue2b, sub2true, reg2true,  mul2true,  comp2true,  mux2true,  shr2true,  shl2true,  div2true,  inc2true,  dec2true, mod2true;
    int addtrue8b, sub8true, reg8true,  mul8true,  comp8true,  mux8true,  shr8true,  shl8true,  div8true,  inc8true,  dec8true, mod8true;
    int addtrue16b, sub16true, reg16true, mul16true, comp16true, mux16true, shr16true, shl16true, div16true, inc16true, dec16true, mod16true;
    int addtrue32b, sub32true, reg32true, mul32true, comp32true, mux32true, shr32true, shl32true, div32true, inc32true, dec32true, mod32true;
    int addtrue64b, sub64true, reg64true, mul64true, comp64true, mux64true, shr64true, shl64true, div64true, inc64true, dec64true, mod64true;

};

//CLASS OBJECTS
COUNTVAL value;
COUNTVAL Boolean;
COUNTVAL FinalValuesCC;
totals   totalvalscomps;
finalvalues finalCalculatedValues;

//GETTER DEPENDENT ON SOME FLAGS IN PARSERS.CPP and PARSER.hpp
//GETTERS/////////////////////////////////////////
double finalvalues::addertotalsF(double adderFinalW)
{
    double a = adderFinalW;
    FinalValuesCC.finaADD = a;
    return adderFinalW;
}

double finalvalues::regstotalsF(double regFinalW)
{
    double a = regFinalW;
    FinalValuesCC.finREG = a;
    return regFinalW;
}

double finalvalues::subtotalsF(double subFinalW)
{
    double a = subFinalW;
    FinalValuesCC.finSUB = a;
    return subFinalW;
}

double finalvalues::multotalsF(double mulFinalW)
{
    double a = mulFinalW;
    FinalValuesCC.finMUL = a;
    return mulFinalW;
}

double finalvalues::comptotalsF(double compFinalW)
{
    double a = compFinalW;
    FinalValuesCC.finMUL = a;
    return compFinalW;
}

double finalvalues::muxtotalsF(double muxFinalW)
{
    double a = muxFinalW;
    FinalValuesCC.finMUX = a;
    return muxFinalW;
}

double finalvalues::shrtotalsF(double shrFinalW)
{
    double a = shrFinalW;
    FinalValuesCC.finSHR = a;
    return shrFinalW;
}

double finalvalues::divtotalsF(double divFinalW)
{
    double a = divFinalW;
    FinalValuesCC.finDIV = a;
    return divFinalW;
}

double finalvalues::shltotalsF(double shlFinalW)
{
    double a = shlFinalW;
    FinalValuesCC.finSHL = a;
    return shlFinalW;
}

double finalvalues::modtotalsF(double modFinalW)
{
    double a = modFinalW;
    FinalValuesCC.finMOD = a;
    return modFinalW;
}

double finalvalues::inctotalsF(double incFinalW)
{
    double a = incFinalW;
    FinalValuesCC.finINC = a;
    return incFinalW;
}

double finalvalues::dectotalsF(double decFinalW)
{
    double a = decFinalW;
    FinalValuesCC.finDEC = a;
    return decFinalW;
}

double totals::addertotals(double adderw1)
{
    double a = adderw1;
    Boolean.connADD1 = a;
    return adderw1;
}

double totals::regstotals(double regw1)
{
    double a = regw1;
    Boolean.connreg1 = a;
    return regw1;
}

double totals::subtotals(double subw1)
{
    double a = subw1;
    Boolean.connsub1 = a;
    return subw1;
}

double totals::multotals(double mulw1)
{
    double a = mulw1;
    Boolean.connmul1 = a;
    return mulw1;
}

double totals::comptotals(double compw1)
{
    double a = compw1;
    Boolean.conComp1 = a;
    return compw1;
}

double totals::muxtotals(double muxw1)
{
    double a = muxw1;
    Boolean.conmux1 = a;
    return muxw1;
}

double totals::shrtotals(double shrw1)
{
    double a = shrw1;
    Boolean.conshr1 = a;
    return shrw1;
}

double totals::shltotals(double shlw1)
{
    double a = shlw1;
    Boolean.conshl1 = a;
    return shlw1;
}

double totals::divtotals(double divw1)
{
    double a = divw1;
    Boolean.condiv1 = a;
    return divw1;
}

double totals::modtotals(double modw1)
{
    double a = modw1;
    Boolean.conmod1 = a;
    return modw1;
}

double totals::inctotals(double incw1)
{
    double a = incw1;
    Boolean.coninc1 = a ;
    return incw1;
}

double totals::dectotals(double decw1)
{
    double a = decw1;
    Boolean.condec1 = a;
    return decw1;
}

int booleanbit::connectionADDtrue(int addtrue)
{
    int a = addtrue;
    Boolean.connADD = a;
    return addtrue;
}

int booleanbit::connectionREGtrue(int regtrue)
{
    int a = regtrue;
    Boolean.connreg = a;
    return regtrue;
}

int booleanbit::connectionSUBtrue(int subtrue)
{
    int a = subtrue;
    Boolean.connsub = a;
    return subtrue;
}

int booleanbit::connectionMULtrue(int multrue)
{
    int a = multrue;
    Boolean.connmul = a;
    return multrue;
}

int booleanbit::connectionCOMPtrue(int comptrue)
{
    int a = comptrue;
    Boolean.conComp = a;
    return comptrue;
}

int booleanbit::connectionMUXtrue(int muxtrue)
{
    int a = muxtrue;
    Boolean.conmux = a;
    return muxtrue;
}

int booleanbit::connectionSHRtrue(int shrtrue)
{
    int a = shrtrue;
    Boolean.conshr = a;
    return shrtrue;
}

int booleanbit::connectionSHLtrue(int shltrue)
{
    int a = shltrue;
    Boolean.conshl = a;
    return shltrue;
}

int booleanbit::connectionDIVtrue(int divtrue)
{
    int a = divtrue;
    Boolean.condiv = a;
    return divtrue;
}

int booleanbit::connectionMODtrue(int modtrue)
{
    int a = modtrue;
    Boolean.conmod = a;
    return modtrue;
}

int booleanbit::connectionINCtrue(int inctrue)
{
    int a = inctrue;
    Boolean.coninc = a;
    return inctrue;
}

int booleanbit::connectionDECtrue(int dectrue)
{
    int a = dectrue;
    Boolean.condec= a;
    return dectrue;
}

int booleanbit::adder1btrue(int addertrue1)
{
    int a = addertrue1;
    Boolean.addtrue1b = a;
    return addertrue1;
}

int booleanbit::adder2btrue(int addertrue2)
{
    int b = addertrue2;
    Boolean.addtrue2b = b;
    return addertrue2;
}

int booleanbit::adder8btrue(int addertrue3)
{
    int c = addertrue3;
    Boolean.addtrue8b = c;
    return addertrue3;
}

int booleanbit::adder16btrue(int addertrue4)
{
    int d = addertrue4;
    Boolean.addtrue16b = d;
    return addertrue4;
}

int booleanbit::adder32btrue(int addertrue5)
{
    int e = addertrue5;
    Boolean.addtrue32b = e;
    return addertrue5;
}

int booleanbit::adder64btrue(int addertrue6)
{
    int f = addertrue6;
    Boolean.addtrue64b = f;
    return addertrue6;
}

int booleanbit::sub1true(int subtrue)
{
    int a = subtrue;
    Boolean.sub1true = a;
    return subtrue;
}

int booleanbit::sub2true(int subtrue)
{
    int a = subtrue;
    Boolean.sub2true = a;
    return subtrue;
}

int booleanbit::sub8true(int subtrue)
{
    int a = subtrue;
    Boolean.sub8true = a;
    return subtrue;
}

int booleanbit::sub16true(int subtrue)
{
    int a = subtrue;
    Boolean.sub16true = a;
    return subtrue;
}

int booleanbit::sub32true(int subtrue)
{
    int a = subtrue;
    Boolean.sub32true = a;
    return subtrue;
}

int booleanbit::sub64true(int subtrue)
{
    int a = subtrue;
    Boolean.sub64true = a;
    return subtrue;
}

int booleanbit::mul1true(int multrue)
{
    int a = multrue ;
    Boolean.mul1true = a;
    return multrue;
}

int booleanbit::mul2true(int multrue)
{
    int a = multrue;
    Boolean.mul2true = a;
    return multrue;
}

int booleanbit::mul8true(int multrue)
{
    int a = multrue;
    Boolean.mul8true = a;
    return multrue;
}

int booleanbit::mul16true(int multrue)
{
    int a = multrue;
    Boolean.mul16true = a;
    return multrue;
}

int booleanbit::mul32rue(int multrue)
{
    int a = multrue;
    Boolean.mul32true = a;
    return multrue;
}

int booleanbit::mul64true(int multrue)
{
    int a = multrue;
    Boolean.mul64true = a;
    return multrue;
}

int booleanbit::comp1true(int comptrue)
{
    int a = comptrue;
    Boolean.comp1true = a;
    return comptrue;
}

int booleanbit::comp2true(int comptrue)
{
    int a = comptrue;
    Boolean.comp2true = a;
    return comptrue;
}

int booleanbit::comp8true(int comptrue)
{
    int a = comptrue;
    Boolean.comp8true = a;
    return comptrue;
}

int booleanbit::comp16true(int comptrue)
{
    int a = comptrue;
    Boolean.comp16true = a;
    return comptrue;
}

int booleanbit::comp32true(int comptrue)
{
    int a = comptrue;
    Boolean.comp32true = a;
    return comptrue;
}

int booleanbit::comp64true(int comptrue)
{
    int a = comptrue;
    Boolean.comp64true = a;
    return comptrue;
}

int booleanbit::mux1true(int muxtrue)
{
    int a = muxtrue;
    Boolean.mux1true = a;
    return muxtrue;
}

int booleanbit::mux2true(int muxtrue)
{
    int a = muxtrue;
    Boolean.mux2true = a;
    return muxtrue;
}

int booleanbit::mux8true(int muxtrue)
{
    int a = muxtrue;
    Boolean.mux8true = a;
    return muxtrue;
}

int booleanbit::mux16true(int muxtrue)
{
    int a = muxtrue;
    Boolean.mux16true = a;
    return muxtrue;
}

int booleanbit::mux32true(int muxtrue)
{
    int a = muxtrue;
    Boolean.mux32true = a;
    return muxtrue;
}

int booleanbit::mux64true(int muxtrue)
{
    int a = muxtrue;
    Boolean.mux64true = a;
    return muxtrue;
}

int booleanbit::shr1true(int shrtrue)
{
    int a = shrtrue;
    Boolean.shr1true = a;
    return shrtrue;
}

int booleanbit::shr2true(int shrtrue)
{
    int a = shrtrue;
    Boolean.shr2true = a;
    return shrtrue;
}

int booleanbit::shr8true(int shrtrue)
{
    int a = shrtrue;
    Boolean.shr8true = a;
    return shrtrue;
}

int booleanbit::shr16true(int shrtrue)
{
    int a = shrtrue;
    Boolean.shr16true = a;
    return shrtrue;
}

int booleanbit::shr32true(int shrtrue)
{
    int a = shrtrue;
    Boolean.shr32true = a;
    return shrtrue;
}

int booleanbit::shr64true(int shrtrue)
{
    int a = shrtrue;
    Boolean.shr64true = a;
    return shrtrue;
}

int booleanbit::shl1true (int shltrue)
{
    int a = shltrue;
    Boolean.shl1true = a;
    return shltrue;
}

int booleanbit::shl2true (int shltrue)
{
    int a = shltrue;
    Boolean.shl2true = a;
    return shltrue;
}

int booleanbit::shl8true (int shltrue)
{
    int a = shltrue;
    Boolean.shl8true = a;
    return shltrue;
}

int booleanbit::shl16true (int shltrue)
{
    int a = shltrue;
    Boolean.shl16true = a;
    return shltrue;
}

int booleanbit::shlt32rue(int shltrue)
{
    int a = shltrue;
    Boolean.shl32true = a;
    return shltrue;
}

int booleanbit::shlt64rue (int shltrue)
{
    int a = shltrue;
    Boolean.shl64true = a;
    return shltrue;
}

int booleanbit::div1true(int divtrue)
{
    int a = divtrue;
    Boolean.div1true = a;
    return divtrue;
}

int booleanbit::div2true(int divtrue)
{
    int a = divtrue;
    Boolean.div2true = a;
    return divtrue;
}

int booleanbit::div8true(int divtrue)
{
    int a = divtrue;
    Boolean.div8true = a;
    return divtrue;
}

int booleanbit::div16true(int divtrue)
{
    int a = divtrue;
    Boolean.div16true = a;
    return divtrue;
}

int booleanbit::div32true(int divtrue)
{
    int a = divtrue;
    Boolean.div32true = a;
    return divtrue;
}

int booleanbit::div64true(int divtrue)
{
    int a = divtrue;
    Boolean.div64true = a;
    return divtrue;
}

int booleanbit::mod1true(int modtrue)
{
    int a = modtrue;
    Boolean.mod1true = a;
    return modtrue;
}

int booleanbit::mod2true(int modtrue)
{
    int a = modtrue;
    Boolean.mod2true = a;
    return modtrue;
}

int booleanbit::mod8true(int modtrue)
{
    int a = modtrue;
    Boolean.mod8true = a;
    return modtrue;
}

int booleanbit::mod16true(int modtrue)
{
    int a = modtrue;
    Boolean.mod16true = a;
    return modtrue;
}

int booleanbit::mod32true(int modtrue)
{
    int a = modtrue;
    Boolean.mod32true = a;
    return modtrue;
}

int booleanbit::mod64true(int modtrue)
{
    int a = modtrue;
    Boolean.mod64true = a;
    return modtrue;
}

int booleanbit::inc1true(int inctrue)
{
    int a = inctrue;
    Boolean.inc1true = a;
    return inctrue;
}

int booleanbit::inc2true(int inctrue)
{
    int a = inctrue;
    Boolean.inc2true = a;
    return inctrue;
}

int booleanbit::inc8true(int inctrue)
{
    int a = inctrue;
    Boolean.inc8true = a;
    return inctrue;
}

int booleanbit::inc16true(int inctrue)
{
    int a = inctrue;
    Boolean.inc16true = a;
    return inctrue;
}

int booleanbit::inc32true(int inctrue)
{
    int a = inctrue;
    Boolean.inc32true = a;
    return inctrue;
}

int booleanbit::inc64true(int inctrue)
{
    int a = inctrue;
    Boolean.inc64true = a;
    return inctrue;
}

int booleanbit::dec1true(int dectrue)
{
    int a = dectrue;
    Boolean.dec1true = a;
    return dectrue;
}

int booleanbit::dec2true(int dectrue)
{
    int a = dectrue;
    Boolean.dec2true = a;
    return dectrue;
}

int booleanbit::dec8true(int dectrue)
{
    int a = dectrue;
    Boolean.dec8true = a;
    return dectrue;
}

int booleanbit::dec16true(int dectrue)
{
    int a = dectrue;
    Boolean.dec16true = a;
    return dectrue;
}

int booleanbit::dec32true(int dectrue)
{
    int a = dectrue;
    Boolean.dec32true = a;
    return dectrue;
}

int booleanbit::dec64true(int dectrue)
{
    int a = dectrue;
    Boolean.dec64true = a;
    return dectrue;
}

int booleanbit::reg1true(int regtrue)
{
    int a = regtrue;
    Boolean.reg1true = a;
    return regtrue;
}

int booleanbit::reg2true(int regtrue)
{
    int a = regtrue;
    Boolean.reg2true = a;
    return regtrue;
}

int booleanbit::reg8true(int regtrue)
{
    int a = regtrue;
    Boolean.reg8true = a;
    return regtrue;
}

int booleanbit::reg16true(int regtrue)
{
    int a = regtrue;
    Boolean.reg16true = a;
    return regtrue;
}

int booleanbit::reg32true(int regtrue)
{
    int a = regtrue;
    Boolean.reg32true = a;
    return regtrue;
}

int booleanbit::reg64true(int regtrue)
{
    int a = regtrue;
    Boolean.reg64true = a;
    return regtrue;
}

int INPUTCount::inputcount(int InptCount)
{
    int a = InptCount;
    value.input = a;
    return InptCount;
}

int WIRECount::wirecount(int Wirecnt)
{
    int b = Wirecnt;
    value.wire = b;
    return Wirecnt;
}

int OUTPUTCount::outputcount(int OutputCNT)
{
    int b = OutputCNT;
    value.output = b;
    return OutputCNT;
}

int componentADDCount::addercount(int adderCount)
{
    int c = adderCount;
    value.Adder = c;
    return adderCount;
}

int componentSUBCount::subcount(int Subcount)
{
    int d = Subcount;
    value.Sub = d;
    return Subcount;
}

int componentMULCount::mulcount(int Mulcount)
{
    int e = Mulcount;
    value.Mul = e;
    return Mulcount;
}

int componentCOMPCount::compcount(int Compcount)
{
    int f = Compcount;
    value.Comp = f;
    return Compcount;
}

int componentMUXCount::muxcount(int Muxcount)
{
    int g = Muxcount;
    value.Mux = g;
    return Muxcount;
}

int componentSHRCount::shrcount(int Shrcount)
{
    int f = Shrcount;
    value.Shr = f;
    return Shrcount;
}

int componentSHLCount::shlcount(int Shlcount)
{
    int g = Shlcount;
    value.Shl = g;
    return Shlcount;
}

int componentDIVCount::divcount(int Divcount)
{
    int h = Divcount;
    value.Div = h;
    return Divcount;
}

int componentMODCount::modcount(int modcount)
{
    int j = modcount;
    value.Mod = j;
    return modcount;
}

int componentINCCount::inccount(int Inccount)
{
    int k = Inccount;
    value.Inc = k;
    return Inccount;
}

int componentDECCount::deccount(int deccount)
{
    int l = deccount;
    value.Dec = l;
    return deccount;
}

int componentREGCount::regcount(int Regcount)
{
    int m = Regcount;
    value.Reg = m;
    return Regcount;
}
//GETTERS/////////////////////////////////////////

//OBJECTS
dataType                weight;
componentAdderWeight    adderweight;
componentSUBWeight      subweight;
componentMULWeight      mulweight;
componentCOMPWeight     compweight;
componentMUX2x1Weight   mux2x1weight;
componentSHRWeight      shrweight;
componentSHLWeight      shlweight;
componentDIVWeight      divweight;
componentMODWeight      modweight;
componentINCWeight      incweight;
componentDECWeight      decweight;
componentREGWeight      regweight;

//NEEDS MODIFY FOR CMD LINE ARG/////////////////////
int main(int argc, const char * argv[])
{
    std::ifstream myfile("474a_circuit1.txt");
    if(myfile.is_open())
    {
        std::cout << "File is open" << std::endl;


    }
    else
    {
        std::cout << "Try a different directory" << std::endl;
    }

    parseFile(myfile);
////////////////////////////////////////////////////

//Passing Values to GRAPH.HPP for ADDER LATENCIES
    adderweight.adder1bweight    = weight.add1b;
    adderweight.adder2bweight    = weight.add2b;
    adderweight.adder8bweight    = weight.add8b;
    adderweight.adder16bweight   = weight.add16b;
    adderweight.adder32weight    = weight.add32b;
    adderweight.adder64bweight   = weight.add64b;

//Passing Values to GRAPH.HPP for SUB LATENCIES
    subweight.SUB1bweight        = weight.sub1b;
    subweight.SUB2bweight        = weight.sub2b;
    subweight.SUB8bweight        = weight.sub8b;
    subweight.SUB16bweight       = weight.sub16b;
    subweight.SUB32weight        = weight.sub32b;
    subweight.SUB64bweight       = weight.sub64b;

//Passing Values to GRAPH.HPP for MUL LATENCIES
    mulweight.MUL1bweight        = weight.mult1b;
    mulweight.MUL2bweight        = weight.mult2b;
    mulweight.MUL8bweight        = weight.mult8b;
    mulweight.MUL16bweight       = weight.mult16b;
    mulweight.MUL32weight        = weight.mult32b;
    mulweight.MUL64bweight       = weight.mult64b;

//Passing Values to GRAPH.HPP for COMP LATENCIES
    compweight.COMP1bweight      = weight.comp1b;
    compweight.COMP2bweight      = weight.comp2b;
    compweight.COMP8bweight      = weight.comp8b;
    compweight.COMP16bweight     = weight.comp16b;
    compweight.COMP32weight      = weight.comp32b;
    compweight.COMP64bweight     = weight.comp64b;

//Passing Values to GRAPH.HPP for MUX2x1 LATENCIES
    mux2x1weight.MUX2x11bweight  = weight.mux1b;
    mux2x1weight.MUX2x12bweight  = weight.mux2b;
    mux2x1weight.MUX2x18bweight  = weight.mux8b;
    mux2x1weight.MUX2x16bweight  = weight.mux16b;
    mux2x1weight.MUX2x132weight  = weight.mux32b;
    mux2x1weight.MUX2x164bweight = weight.mux64b;

//Passing Values to GRAPH.HPP for SHR LATENCIES
    shrweight.SHR1bweight        = weight.shr1b;
    shrweight.SHR2bweight        = weight.shr2b;
    shrweight.SHR8bweight        = weight.shr8b;
    shrweight.SHR16bweight       = weight.shr16b;
    shrweight.SHR32weight        = weight.shr32b;
    shrweight.SHR64bweight       = weight.shr64b;

//Passing Values to GRAPH.HPP for SHL LATENCIES
    shlweight.SHL1bweight        = weight.shl1b;
    shlweight.SHL2bweight        = weight.shl2b;
    shlweight.SHL8bweight        = weight.shl8b;
    shlweight.SHL16bweight       = weight.shl16b;
    shlweight.SHL32weight        = weight.shl32b;
    shlweight.SHL64bweight       = weight.shl64b;

//Passing Values to GRAPH.HPP for DIV LATENCIES
    divweight.DIV1bweight        = weight.div1b;
    divweight.DIV2bweight        = weight.div2b;
    divweight.DIV8bweight        = weight.div8b;
    divweight.DIV16bweight       = weight.div16b;
    divweight.DIV32weight        = weight.div32b;
    divweight.DIV64bweight       = weight.div64b;

//Passing Values to GRAPH.HPP for MOD LATENCIES
    modweight.MOD1bweight       = weight.mod1b;
    modweight.MOD2bweight       = weight.mod2b;
    modweight.MOD8bweight       = weight.mod8b;
    modweight.MOD16bweight      = weight.mod16b;
    modweight.MOD32weight       = weight.mod32b;
    modweight.MOD64bweight      = weight.mod64b;

//Passing Values to GRAPH.HPP for INC LATENCIES
    incweight.INC1bweight       = weight.inc1b;
    incweight.INC2bweight       = weight.inc2b;
    incweight.INC8bweight       = weight.inc8b;
    incweight.INC16bweight      = weight.inc16b;
    incweight.INC32weight       = weight.inc32b;
    incweight.INC64bweight      = weight.inc64b;

//Passing Values to GRAPH.HPP for DEC LATENCIES
    decweight.DEC1bweight       = weight.dec1b;
    decweight.DEC2bweight       = weight.dec2b;
    decweight.DEC8bweight       = weight.dec8b;
    decweight.DEC16bweight      = weight.dec16b;
    decweight.DEC32weight       = weight.dec32b;
    decweight.DEC64bweight      = weight.dec64b;

//Passing Values to GRAPH.HPP for REG LATENCIES
    regweight.REG1bweight       = weight.reg1b;
    regweight.REG2bweight       = weight.reg2b;
    regweight.REG8bweight       = weight.reg8b;
    regweight.REG16bweight      = weight.reg16b;
    regweight.REG32weight       = weight.reg32b;
    regweight.REG64bweight      = weight.reg64b;


//PASSING ADDERS TO GRAPH
    if(value.addtrue1b == 1)
    {
        criticalPathCalcComps(value.addtrue1b,  "adder1",adderweight.adder1bweight,value.Adder);
    }
    else if(Boolean.addtrue2b == 1)
    {
        criticalPathCalcComps(Boolean.addtrue2b,"adder2",adderweight.adder2bweight,value.Adder);
    }
    else if(Boolean.addtrue8b == 1)
    {
        criticalPathCalcComps(Boolean.addtrue8b, "adder8",adderweight.adder8bweight,value.Adder);
    }
    else if(Boolean.addtrue16b == 1)
    {
        criticalPathCalcComps(Boolean.addtrue16b,"adder16",adderweight.adder16bweight, value.Adder);
    }
    else if (Boolean.addtrue32b == 1)
    {
        criticalPathCalcComps(Boolean.addtrue32b,"adder32",adderweight.adder32weight,value.Adder);
    }
    else if(Boolean.addtrue64b == 1)
    {
        criticalPathCalcComps(Boolean.addtrue64b,"adder64",adderweight.adder64bweight,value.Adder);
    }

//PASSING REG TO GRAPH
    if(value.reg1true == 1)
    {
        criticalPathCalcComps(value.reg1true,  "reg1",regweight.REG1bweight,value.Reg);
    }
    else if(Boolean.reg2true == 1)
    {
        criticalPathCalcComps(Boolean.reg2true,"reg2",regweight.REG1bweight,value.Reg);
    }
    else if(Boolean.reg8true == 1)
    {
        criticalPathCalcComps(Boolean.reg8true, "reg8",regweight.REG1bweight,value.Reg);
    }
    else if(Boolean.reg16true == 1)
    {
        criticalPathCalcComps(Boolean.reg16true,"reg16",regweight.REG1bweight, value.Reg);
    }
    else if (Boolean.reg32true == 1)
    {
        criticalPathCalcComps(Boolean.reg32true,"reg32",regweight.REG1bweight,value.Reg);
    }
    else if(Boolean.reg64true == 1)
    {
        criticalPathCalcComps(Boolean.reg64true,"reg64",regweight.REG1bweight,value.Reg);
    }

//PASSING SUB TO GRAPH
    if(value.sub1true == 1)
    {
        criticalPathCalcComps(value.sub1true,  "sub1",subweight.SUB1bweight,value.Sub);
    }
    else if(Boolean.sub2true == 1)
    {
        criticalPathCalcComps(Boolean.sub2true,"sub2",subweight.SUB2bweight,value.Sub);
    }
    else if(Boolean.sub8true == 1)
    {
        criticalPathCalcComps(Boolean.sub8true, "sub8",subweight.SUB8bweight,value.Sub);
    }
    else if(Boolean.sub16true== 1)
    {
        criticalPathCalcComps(Boolean.sub16true,"sub16",subweight.SUB16bweight, value.Sub);
    }
    else if (Boolean.sub32true == 1)
    {
        criticalPathCalcComps(Boolean.sub32true,"sub32",subweight.SUB32weight,value.Sub);
    }
    else if(Boolean.sub64true == 1)
    {
        criticalPathCalcComps(Boolean.sub64true,"sub64",subweight.SUB64bweight,value.Sub);
    }

//PASSING MULS TO GRAPH
    if(value.mul1true == 1)
    {
        criticalPathCalcComps(value.mul1true,  "mul1",mulweight.MUL1bweight,value.Reg);
    }
    else if(Boolean.mul2true == 1)
    {
        criticalPathCalcComps(Boolean.mul2true,"mul2",mulweight.MUL2bweight,value.Reg);
    }
    else if(Boolean.mul8true == 1)
    {
        criticalPathCalcComps(Boolean.mul8true, "mul18",mulweight.MUL8bweight,value.Reg);
    }
    else if(Boolean.mul16true == 1)
    {
        criticalPathCalcComps(Boolean.mul16true,"mul16",mulweight.MUL16bweight, value.Reg);
    }
    else if (Boolean.mul32true  == 1)
    {
        criticalPathCalcComps(Boolean.mul32true,"mul32",mulweight.MUL32weight,value.Reg);
    }
    else if(Boolean.mul64true  == 1)
    {
        criticalPathCalcComps(Boolean.mul64true,"mul64",mulweight.MUL64bweight,value.Reg);
    }

//PASSING COMPS TO GRAPH
    if(value.comp1true == 1)
    {
        criticalPathCalcComps(value.comp1true,  "comp1",compweight.COMP1bweight,value.Comp);
    }
    else if(Boolean.comp2true == 1)
    {
        criticalPathCalcComps(Boolean.comp2true,"comp2",compweight.COMP2bweight,value.Comp);
    }
    else if(Boolean.comp8true == 1)
    {
        criticalPathCalcComps(Boolean.comp8true, "comp8",compweight.COMP8bweight,value.Comp);
    }
    else if(Boolean.comp16true == 1)
    {
        criticalPathCalcComps(Boolean.comp16true,"comp16",compweight.COMP16bweight, value.Comp);
    }
    else if (Boolean.comp32true  == 1)
    {
        criticalPathCalcComps(Boolean.comp32true,"comp32",compweight.COMP32weight,value.Comp);
    }
    else if(Boolean.comp64true  == 1)
    {
        criticalPathCalcComps(Boolean.comp64true,"comp64",compweight.COMP64bweight,value.Comp);
    }

//PASSING MUX TO GRAPH
    if(value.mux1true == 1)
    {
        criticalPathCalcComps(value.mux1true,"mux1",mux2x1weight.MUX2x11bweight,value.Mux);
    }
    else if(Boolean.mux2true == 1)
    {
        criticalPathCalcComps(Boolean.mux2true,"mux2",mux2x1weight.MUX2x12bweight,value.Mux);
    }
    else if(Boolean.mux8true == 1)
    {
        criticalPathCalcComps(Boolean.mux8true, "mux8",mux2x1weight.MUX2x18bweight,value.Mux);
    }
    else if(Boolean.mux16true == 1)
    {
        criticalPathCalcComps(Boolean.mux16true,"mux16",mux2x1weight.MUX2x16bweight, value.Mux);
    }
    else if (Boolean.mux32true  == 1)
    {
        criticalPathCalcComps(Boolean.mux32true,"mux32",mux2x1weight.MUX2x132weight,value.Mux);
    }
    else if(Boolean.mux64true  == 1)
    {
        criticalPathCalcComps(Boolean.mux64true,"mux64",mux2x1weight.MUX2x164bweight,value.Mux);
    }

//PASSING SHR TO GRAPH
    if(value.shr1true == 1)
    {
        criticalPathCalcComps(value.shr1true,"shr1",shrweight.SHR1bweight,value.Shr);
    }
    else if(Boolean.shr2true == 1)
    {
        criticalPathCalcComps(Boolean.shr2true,"shr2",shrweight.SHR2bweight,value.Mux);
    }
    else if(Boolean.shr8true == 1)
    {
        criticalPathCalcComps(Boolean.shr8true, "shr8",shrweight.SHR8bweight,value.Mux);
    }
    else if(Boolean.shr16true == 1)
    {
        criticalPathCalcComps(Boolean.shr16true,"shr16",shrweight.SHR16bweight, value.Mux);
    }
    else if (Boolean.shr32true  == 1)
    {
        criticalPathCalcComps(Boolean.shr32true,"shr32",shrweight.SHR32weight,value.Mux);
    }
    else if(Boolean.shr64true  == 1)
    {
        criticalPathCalcComps(Boolean.shr64true,"shr64",shrweight.SHR64bweight,value.Mux);
    }

//PASSING SHL to GRAPH
    if(value.shl1true == 1)
    {
        criticalPathCalcComps(value.shl1true,"shl1",shlweight.SHL1bweight,value.Shl);
    }
    else if(Boolean.shl2true == 1)
    {
        criticalPathCalcComps(Boolean.shl2true,"shl2",shlweight.SHL2bweight,value.Shl);
    }
    else if(Boolean.shl8true == 1)
    {
        criticalPathCalcComps(Boolean.shl8true, "shl8",shlweight.SHL8bweight,value.Shl);
    }
    else if(Boolean.shl16true == 1)
    {
        criticalPathCalcComps(Boolean.shl16true,"shl16",shlweight.SHL16bweight, value.Shl);
    }
    else if (Boolean.shl32true  == 1)
    {
        criticalPathCalcComps(Boolean.shl32true,"shl32",shlweight.SHL32weight,value.Shl);
    }
    else if(Boolean.shl64true  == 1)
    {
        criticalPathCalcComps(Boolean.shl64true,"shl64",shlweight.SHL64bweight,value.Shl);
    }

//PASSING  DIV TO GRAPH
    if(value.div1true == 1)
    {
        criticalPathCalcComps(value.div1true,"div1",divweight.DIV1bweight,value.Div);
    }
    else if(Boolean.div2true == 1)
    {
        criticalPathCalcComps(Boolean.div2true,"div2",divweight.DIV2bweight,value.Div);
    }
    else if(Boolean.div8true == 1)
    {
        criticalPathCalcComps(Boolean.div8true, "div8",divweight.DIV8bweight,value.Div);
    }
    else if(Boolean.div16true == 1)
    {
        criticalPathCalcComps(Boolean.div16true,"div16",divweight.DIV16bweight, value.Div);
    }
    else if (Boolean.div32true  == 1)
    {
        criticalPathCalcComps(Boolean.div32true,"div32",divweight.DIV32weight,value.Div);
    }
    else if(Boolean.div64true  == 1)
    {
        criticalPathCalcComps(Boolean.div64true,"div64",divweight.DIV64bweight,value.Div);
    }

//PASSING MOD TO GRAPH
    if(value.mod1true == 1)
    {
        criticalPathCalcComps(value.mod1true,"mod1",modweight.MOD1bweight,value.Mod);
    }
    else if(Boolean.mod2true == 1)
    {
        criticalPathCalcComps(Boolean.mod2true,"mod2",modweight.MOD2bweight,value.Mod);
    }
    else if(Boolean.mod8true == 1)
    {
        criticalPathCalcComps(Boolean.mod8true, "mod8",modweight.MOD8bweight,value.Mod);
    }
    else if(Boolean.mod16true == 1)
    {
        criticalPathCalcComps(Boolean.mod16true,"mod16",modweight.MOD16bweight, value.Mod);
    }
    else if (Boolean.mod32true  == 1)
    {
        criticalPathCalcComps(Boolean.mod32true,"mod32",modweight.MOD32weight,value.Mod);
    }
    else if(Boolean.mod64true  == 1)
    {
        criticalPathCalcComps(Boolean.mod64true,"mod64",modweight.MOD64bweight,value.Mod);
    }

//PASSING INC TO GRAPH
    if(value.inc1true == 1)
    {
        criticalPathCalcComps(value.inc1true,"inc1",incweight.INC1bweight,value.Inc);
    }
    else if(Boolean.inc2true == 1)
    {
        criticalPathCalcComps(Boolean.inc2true,"inc2",incweight.INC2bweight,value.Inc);
    }
    else if(Boolean.inc8true == 1)
    {
        criticalPathCalcComps(Boolean.inc8true, "inc8",incweight.INC8bweight,value.Inc);
    }
    else if(Boolean.inc16true == 1)
    {
        criticalPathCalcComps(Boolean.inc16true,"inc16",incweight.INC16bweight, value.Inc);
    }
    else if (Boolean.inc32true  == 1)
    {
        criticalPathCalcComps(Boolean.inc32true,"inc32",incweight.INC32weight,value.Inc);
    }
    else if(Boolean.inc64true  == 1)
    {
        criticalPathCalcComps(Boolean.inc64true,"inc64",incweight.INC64bweight,value.Inc);
    }

//PASSING DEC TO GRAPH
    if(value.dec1true == 1)
    {
        criticalPathCalcComps(value.dec1true,"dec1",decweight.DEC1bweight,value.Dec);
    }
    else if(Boolean.dec2true == 1)
    {
        criticalPathCalcComps(Boolean.dec2true,"dec2",decweight.DEC2bweight,value.Dec);
    }
    else if(Boolean.dec8true == 1)
    {
        criticalPathCalcComps(Boolean.dec8true, "dec8",decweight.DEC8bweight,value.Dec);
    }
    else if(Boolean.dec16true == 1)
    {
        criticalPathCalcComps(Boolean.dec16true,"de16",decweight.DEC16bweight, value.Dec);
    }
    else if (Boolean.dec32true  == 1)
    {
        criticalPathCalcComps(Boolean.dec32true,"dec32",decweight.DEC32weight,value.Dec);
    }
    else if(Boolean.dec64true  == 1)
    {
        criticalPathCalcComps(Boolean.dec64true,"dec64",decweight.DEC64bweight,value.Dec);
    }
//SETS TO TRUE WHEN COMP + CONNECTION is TRUE
//BASICALLY PRINTS A PATH NOT IN ORDER
    if (Boolean.connADD == 1)
    {
        cout<<"ADD-->";
        sortingTOTALSCalc(Boolean.connADD1);
    }
    if (Boolean.connreg ==1)
    {
        cout<<"REG-->";
        sortingTOTALSCalc1(Boolean.connreg1);
    }
    if (Boolean.connsub == 1)
    {
        cout<<"SUB-->";
        sortingTOTALSCalc2(Boolean.connsub1);
    }
    if (Boolean.connmul == 1)
    {
        cout<<"MUL-->";
        sortingTOTALSCalc3(Boolean.connmul1);
    }
    if (Boolean.conComp == 1)
    {
        cout<<"COMp-->";
        sortingTOTALSCalc4(Boolean.conComp1);
    }
    if (Boolean.conmux == 1)
    {
        cout<<"MUX-->";
        sortingTOTALSCalc5(Boolean.conmux1);
    }
    if(Boolean.conshr == 1)
    {
        cout<<"SHR-->";
        sortingTOTALSCalc6(Boolean.conshr1);
    }
    if(Boolean.conshl == 1)
    {
        cout<<"SHLD-->";
        sortingTOTALSCalc7(Boolean.conshl1);
    }
    if(Boolean.condiv == 1)
    {
        cout<<"DIV-->";
        sortingTOTALSCalc8(Boolean.condiv1);
    }
    if(Boolean.conmod == 1 )
    {
        cout<<"MOD-->";
        sortingTOTALSCalc9(Boolean.conmod1);
    }
    if(Boolean.coninc == 1)
    {
        cout<<"INC-->";
        sortingTOTALSCalc10(Boolean.coninc1);
    }
    if(Boolean.condec == 1)
    {
        cout<<"DEC-->";
        sortingTOTALSCalc11(Boolean.condec1);
    }
//FOR CRIT__PATH CALC
    FINALCCalculated(   FinalValuesCC.finaADD,
                        FinalValuesCC.finREG,
                        FinalValuesCC.finSUB,
                        FinalValuesCC.finMUL,
                        FinalValuesCC.finCMP,
                        FinalValuesCC.finMUX,
                        FinalValuesCC.finSHR,
                        FinalValuesCC.finSHL,
                        FinalValuesCC.finDIV,
                        FinalValuesCC.finMOD,
                        FinalValuesCC.finINC,
                        FinalValuesCC.finDEC
                    );

    return 0;
}
