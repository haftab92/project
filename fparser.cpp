//
//  parser.cpp
//  Datapath Generator
//  MODIFIED BY FRANK P. las update 11/16/2020 @ 8:35PM COMPILES FINE
//  ---MAINLY SETTING ,VARIABLES ,FLAGS AND COUNTERS
//  Created by Brady Thomas on 10/28/20.
//

#include "parser.hpp"
#include <fstream>
#include <iostream>
#include <vector>
#include "dpgen.hpp"
#include <string>
#include <sstream>
#include <regex>

//Frank's CODE
int inputcount;
int wirecount;
int outputcount;
int compcount;
int compcountsub;
int compcountmul;
int compcountcomp;
int compcountGTcomp;
int compcountLTcomp;
int compcountmux;
int compcountshr;
int compcountshl;
int compcountdiv;
int compcountmod;
int compcountinc;
int compcountdec;
int compcountreg;
int connectionADD;
int connectionREG;
int connectionSUB;
int connectionMUL;
int connectinCOMP;
int connectionMUX2x1;
int connectionSHR;
int connectionSHL;
int connectionDIV;
int connectionMOD;
int connectionINC;
int connectionDEC;
//FRANK'CODE

//CLASS OBJETCS
//FRANK's code
//Frank's class stuff uses this NOT DELETE
booleanbit          connection;
booleanbit          connectionCOMP;
INPUTCount          inputcnt;
WIRECount           wirecnt;
OUTPUTCount         outcnt;
componentADDCount   adder;
componentSUBCount   sub;
componentMULCount   mul;
componentCOMPCount  comp;
componentMUXCount   mux;
componentSHRCount   shr;
componentSHLCount   shl;
componentDIVCount   div1;
componentMODCount   mod;
componentINCCount   inc;
componentDECCount   dec;
componentREGCount   reg;
//CLASS OBJETCS
//FRANK's code
//Frank's class stuff uses this NOT DELETE


void parseFile(std::ifstream& inFile)
{
    int fileCount = 0;
    std::vector<connectionType> Connections;
    std::vector<component> Components;
    std::string str2Write;
    std::string str;

    // Create connections for Clk and Rst
    connectionType connection;
    connection.Size = "1";
    connection.Name = "Clk";
    connection.output = false;
    connection.input = true;
    connection.wire = false;
    Connections.push_back(connection);
    connection.Size = "1";
    connection.Name = "Rst";
    connection.output = false;
    connection.input = true;
    connection.wire = false;
    Connections.push_back(connection);

    while(std::getline(inFile, str))
    {
        evaluateString(str, &Connections, &Components);
    }



}

// Work on comparator functions!!!
// And if there are size mismatches for inputs to do padding
void evaluateString(std::string& line, std::vector<connectionType> *verilogConnections, std::vector<component> *verilogComponents)
{
    int fileCount = 0;
    int count = 0;
    std::string tempName;
    std::string tempSize;
    std::string const delims{ " ," };
    size_t beg, pos = 0;
    std::vector<std::string> strings;
    std::string tempStr;
    std::string tempStr2;
    std::string tempStr3;
    std::string tempStr4;
    std::string str2Write;

    std::vector<std::string> tempInputs;
    std::vector<std::string> tempOutputs;

    // Assign Inputs
    // URGENT: NEED TO HANDLE SAME CASE IF UNSIGNED
    // Check for errors and comments, might need to figure out a better way to do the register
    if(line.find("input Int") != std::string::npos)
    {
        line.erase(line.begin(), line.begin()+9);
        std::cout << line << std::endl;


        while((beg = line.find_first_not_of(delims, pos)) != std::string::npos)
        {
            pos = line.find_first_of(delims, beg+1);
            if(count == 0)
            {
                tempSize.assign(line.substr(beg, pos-beg));
            }
            else
            {
                connectionType connection;
                connection.Size = tempSize;
                connection.Name = line.substr(beg, pos-beg);
                connection.output = false;
                connection.input = true;
                connection.wire = false;
                verilogConnections->push_back(connection);
                std::cout << verilogConnections->size() << std::endl;
                inputcount++; //Frank's class stuff uses this NOT DELETE

            }
            count++;
        }
        inputcnt.inputcount(inputcount); //Frank's class stuff uses this NOT DELETE
        count = 0;
    }
    // Assign Outputs
    else if(line.find("output Int") != std::string::npos)
    {
        line.erase(line.begin(), line.begin()+10);
        std::cout << line << std::endl;

        while((beg = line.find_first_not_of(delims, pos)) != std::string::npos)
        {
            pos = line.find_first_of(delims, beg+1);
            if(count == 0)
            {
                tempSize.assign(line.substr(beg, pos-beg));
            }
            else
            {
                connectionType connection;
                connection.Size = tempSize;
                connection.Name = line.substr(beg, pos-beg);
                connection.output = true;
                connection.input = false;
                connection.wire = false;
                verilogConnections->push_back(connection);
                std::cout << verilogConnections->size() << std::endl;
                outputcount++;//Frank's class stuff uses this NOT DELETE

            }
            count++;
        }
        outcnt.outputcount(outputcount); //Frank's class stuff uses this NOT DELETE
        count = 0;
    }

    // Assign wires
    else if(line.find("wire Int") != std::string::npos)
    {
        line.erase(line.begin(),line.begin()+8);
        std::cout << line << std::endl;

        while((beg = line.find_first_not_of(delims, pos)) != std::string::npos)
        {
            pos = line.find_first_of(delims, beg+1);
            if(count == 0)
            {
                tempSize.assign(line.substr(beg, pos-beg));
            }
            else
            {
                connectionType connection;
                connection.Size = tempSize;
                connection.Name = line.substr(beg, pos-beg);
                connection.output = false;
                connection.input = false;
                connection.wire = true;
                verilogConnections->push_back(connection);
                std::cout << verilogConnections->size() << std::endl;
                wirecount++; //Frank's class stuff uses this NOT DELETE

            }
            count++;
        }
        wirecnt.wirecount(wirecount);//Frank's class stuff uses this NOT DELETE
        count = 0;
    }

    // Assign components
    else if(line.find("=") != std::string::npos)
    {
        component c1;
        // Adder
        if(line.find("+") != std::string::npos)
        {
            c1.compName = "ADD";
            while((beg = line.find_first_not_of(delims, pos)) != std::string::npos)
            {
                pos = line.find_first_of(delims, beg+1);
                strings.push_back(line.substr(beg, pos-beg));
            }
            tempStr = strings.at(0);
            tempStr2 = strings.at(2);
            tempStr3 = strings.at(4);
            // Find connection
            for(count = 0; count < verilogConnections->size(); count++)
            {
                if(verilogConnections->at(count).Name.compare(tempStr) == 0)
                {
                    tempSize = verilogConnections->at(count).getSize();
                    c1.size = tempSize;
                    c1.latency = setLatencyValues(tempSize, c1.compName);
                    c1.componentOutputs.push_back(verilogConnections->at(count));
                }
                else if(verilogConnections->at(count).Name.compare(tempStr2) == 0)
                {
                    c1.componentInputs.push_back(verilogConnections->at(count));
                }
                else if(verilogConnections->at(count).Name.compare(tempStr3) == 0)
                {
                    c1.componentInputs.push_back(verilogConnections->at(count));
                }
            }
            ++compcount;//Frank's class stuff uses this NOT DELETE
            adder.addercount(compcount);//Frank's class stuff uses this NOT DELETE
            if(tempStr.size()!=0)//Frank's class stuff uses this NOT DELETE
            {
                connection.connectionADDtrue(1);//Frank's class stuff uses this NOT DELETE
            }


        }
        // Subtractor
        else if(line.find("-") != std::string::npos)
        {
            c1.compName = "SUB";
            while((beg = line.find_first_not_of(delims, pos)) != std::string::npos)
            {
                pos = line.find_first_of(delims, beg+1);
                strings.push_back(line.substr(beg, pos-beg));
            }
            tempStr = strings.at(0);
            tempStr2 = strings.at(2);
            tempStr3 = strings.at(4);
            for(count = 0; count < verilogConnections->size(); count++)
            {
                if(verilogConnections->at(count).Name.compare(tempStr) == 0)
                {
                    tempSize = verilogConnections->at(count).getSize();
                    c1.size = tempSize;
                    c1.latency = setLatencyValues(tempSize, c1.compName);
                    c1.componentOutputs.push_back(verilogConnections->at(count));
                }
                else if(verilogConnections->at(count).Name.compare(tempStr2) == 0)
                {
                    c1.componentInputs.push_back(verilogConnections->at(count));
                }
                else if(verilogConnections->at(count).Name.compare(tempStr3) == 0)
                {
                    c1.componentInputs.push_back(verilogConnections->at(count));
                }
            }

            verilogComponents->push_back(c1);
            compcountsub++; //Frank's class stuff uses this NOT DELETE
            sub.subcount(compcountsub);//Frank's class stuff uses this NOT DELETE
            if(tempStr.size()!=0)//Frank's class stuff uses this NOT DELETE
            {
                connection.connectionSUBtrue(1);//Frank's class stuff uses this NOT DELETE
            }
        }
        // Multiplier
        else if(line.find("*") != std::string::npos)
        {
            c1.compName = "MUL";
            while((beg = line.find_first_not_of(delims, pos)) != std::string::npos)
            {
                pos = line.find_first_of(delims, beg+1);
                strings.push_back(line.substr(beg, pos-beg));
            }
            tempStr = strings.at(0);
            tempStr2 = strings.at(2);
            tempStr3 = strings.at(4);
            for(count = 0; count < verilogConnections->size(); count++)
            {
                if(verilogConnections->at(count).Name.compare(tempStr) == 0)
                {
                    tempSize = verilogConnections->at(count).getSize();
                    c1.size = tempSize;
                    c1.latency = setLatencyValues(tempSize, c1.compName);
                    c1.componentOutputs.push_back(verilogConnections->at(count));
                }
                else if(verilogConnections->at(count).Name.compare(tempStr2) == 0)
                {
                    c1.componentInputs.push_back(verilogConnections->at(count));
                }
                else if(verilogConnections->at(count).Name.compare(tempStr3) == 0)
                {
                    c1.componentInputs.push_back(verilogConnections->at(count));
                }
            }
            verilogComponents->push_back(c1);
            compcountmul++;//Frank's class stuff uses this NOT DELETE
            mul.mulcount(compcountmul);//Frank's class stuff uses this NOT DELETE
            if(tempStr.size()!=0)//Frank's class stuff uses this NOT DELETE
            {
                connection.connectionMULtrue(1);//Frank's class stuff uses this NOT DELETE
            }
        }
        // Comparator (GT output)
        else if(line.find(">") != std::string::npos)
        {
            c1.compName = "COMP";
            while((beg = line.find_first_not_of(delims, pos)) != std::string::npos)
            {
                pos = line.find_first_of(delims, beg+1);
                strings.push_back(line.substr(beg, pos-beg));
            }
            tempStr = strings.at(0);
            tempStr2 = strings.at(2);
            tempStr3 = strings.at(4);
            for(count = 0; count < verilogConnections->size(); count++)
            {
                if(verilogConnections->at(count).Name.compare(tempStr) == 0)
                {
                    tempSize = verilogConnections->at(count).getSize();
                    c1.size = tempSize;
                    c1.latency = setLatencyValues(tempSize, c1.compName);
                    c1.componentOutputs.push_back(verilogConnections->at(count));
                }
                else if(verilogConnections->at(count).Name.compare(tempStr2) == 0)
                {
                    c1.componentInputs.push_back(verilogConnections->at(count));
                }
                else if(verilogConnections->at(count).Name.compare(tempStr3) == 0)
                {
                    c1.componentInputs.push_back(verilogConnections->at(count));
                }

            }
            // Create Wire for GT
            connectionType connection;
            connection.Size = tempSize;
            connection.Name = "GT";
            connection.output = false;
            connection.input = false;
            connection.wire = true;
            verilogConnections->push_back(connection);
            c1.componentOutputs.push_back(connection);
            verilogComponents->push_back(c1);

            compcountGTcomp++;//Frank's class stuff uses this NOT DELETE
            comp.compcount(compcountGTcomp);//Frank's class stuff uses this NOT DELETE
            if(tempStr.size()!=0)//Frank's class stuff uses this NOT DELETE
            {
                connectionCOMP.connectionCOMPtrue(1);//Frank's class stuff uses this NOT DELETE
            }
        }
        // Comparator (LT output)
        else if(line.find("<") != std::string::npos)
        {
            c1.compName = "COMP";
            while((beg = line.find_first_not_of(delims, pos)) != std::string::npos)
            {
                pos = line.find_first_of(delims, beg+1);
                strings.push_back(line.substr(beg, pos-beg));
            }
            tempStr = strings.at(0);
            tempStr2 = strings.at(2);
            tempStr3 = strings.at(4);
            for(count = 0; count < verilogConnections->size(); count++)
            {
                if(verilogConnections->at(count).Name.compare(tempStr) == 0)
                {
                    tempSize = verilogConnections->at(count).getSize();
                    c1.size = tempSize;
                    c1.latency = setLatencyValues(tempSize, c1.compName);
                    c1.componentOutputs.push_back(verilogConnections->at(count));
                }
                else if(verilogConnections->at(count).Name.compare(tempStr2) == 0)
                {
                    c1.componentInputs.push_back(verilogConnections->at(count));
                }
                else if(verilogConnections->at(count).Name.compare(tempStr3) == 0)
                {
                    c1.componentInputs.push_back(verilogConnections->at(count));
                }

            }
            // Create Wire for LT
            connectionType connection;
            connection.Size = tempSize;
            connection.Name = "LT";
            connection.output = false;
            connection.input = false;
            connection.wire = true;
            verilogConnections->push_back(connection);
            c1.componentOutputs.push_back(connection);
            verilogComponents->push_back(c1);

            compcountLTcomp++;//Frank's class stuff uses this NOT DELETE
            comp.compcount(compcountLTcomp);//Frank's class stuff uses this NOT DELETE
            if(tempStr.size()!=0)//Frank's class stuff uses this NOT DELETE
            {
                connectionCOMP.connectionCOMPtrue(1);//Frank's class stuff uses this NOT DELETE
            }
        }
        // Comparator (EQ output)
        else if(line.find("==") != std::string::npos)
        {
            c1.compName = "COMP";
            while((beg = line.find_first_not_of(delims, pos)) != std::string::npos)
            {
                pos = line.find_first_of(delims, beg+1);
                strings.push_back(line.substr(beg, pos-beg));
            }
            tempStr = strings.at(0);
            tempStr2 = strings.at(2);
            tempStr3 = strings.at(4);
            for(count = 0; count < verilogConnections->size(); count++)
            {
                if(verilogConnections->at(count).Name.compare(tempStr) == 0)
                {
                    tempSize = verilogConnections->at(count).getSize();
                    c1.size = tempSize;
                    c1.latency = setLatencyValues(tempSize, c1.compName);
                    c1.componentOutputs.push_back(verilogConnections->at(count));
                }
                else if(verilogConnections->at(count).Name.compare(tempStr2) == 0)
                {
                    c1.componentInputs.push_back(verilogConnections->at(count));
                }
                else if(verilogConnections->at(count).Name.compare(tempStr3) == 0)
                {
                    c1.componentInputs.push_back(verilogConnections->at(count));
                }

            }
            // Create Wire for EQ
            connectionType connection;
            connection.Size = tempSize;
            connection.Name = "EQ";
            connection.output = false;
            connection.input = false;
            connection.wire = true;
            verilogConnections->push_back(connection);
            c1.componentOutputs.push_back(connection);
            verilogComponents->push_back(c1);

            compcountcomp++;//Frank's class stuff uses this NOT DELETE
            comp.compcount(compcountcomp);//Frank's class stuff uses this NOT DELETE
            if(tempStr.size()!=0)//Frank's class stuff uses this NOT DELETE
            {
                connectionCOMP.connectionCOMPtrue(1);//Frank's class stuff uses this NOT DELETE
            }

        }
        // MUX2x1
        else if(line.find("?") != std::string::npos)
        {
            c1.compName = "MUX2x1";
            while((beg = line.find_first_not_of(delims, pos)) != std::string::npos)
            {
                pos = line.find_first_of(delims, beg+1);
                strings.push_back(line.substr(beg, pos-beg));
            }
            tempStr = strings.at(0);
            tempStr2 = strings.at(2);
            tempStr3 = strings.at(4);
            tempStr4 = strings.at(6);
            for(count = 0; count < verilogConnections->size(); count++)
            {
                if(verilogConnections->at(count).Name.compare(tempStr) == 0)
                {
                    tempSize = verilogConnections->at(count).getSize();
                    c1.size = tempSize;
                    c1.latency = setLatencyValues(tempSize, c1.compName);
                    c1.componentOutputs.push_back(verilogConnections->at(count));
                }
                else if(verilogConnections->at(count).Name.compare(tempStr2) == 0)
                {
                    c1.componentInputs.push_back(verilogConnections->at(count));
                }
                else if(verilogConnections->at(count).Name.compare(tempStr3) == 0)
                {
                    c1.componentInputs.push_back(verilogConnections->at(count));
                }
                else if(verilogConnections->at(count).Name.compare(tempStr4) == 0)
                {
                    c1.componentInputs.push_back(verilogConnections->at(count));
                }
            }
            verilogComponents->push_back(c1);
            compcountmux++;//Frank's class stuff uses this NOT DELETE
            mux.muxcount(compcountmux);//Frank's class stuff uses this NOT DELETE
            if(tempStr.size()!=0)//Frank's class stuff uses this NOT DELETE
            {
                connectionCOMP.connectionMUXtrue(1);//Frank's class stuff uses this NOT DELETE
            }
        }
        // SHR
        else if(line.find(">>") != std::string::npos)
        {
            c1.compName = "SHR";
            while((beg = line.find_first_not_of(delims, pos)) != std::string::npos)
            {
                pos = line.find_first_of(delims, beg+1);
                strings.push_back(line.substr(beg, pos-beg));
            }
            tempStr = strings.at(0);
            tempStr2 = strings.at(2);
            tempStr3 = strings.at(4);
            for(count = 0; count < verilogConnections->size(); count++)
            {
                if(verilogConnections->at(count).Name.compare(tempStr) == 0)
                {
                    tempSize = verilogConnections->at(count).getSize();
                    c1.size = tempSize;
                    c1.latency = setLatencyValues(tempSize, c1.compName);
                    c1.componentOutputs.push_back(verilogConnections->at(count));
                }
                else if(verilogConnections->at(count).Name.compare(tempStr2) == 0)
                {
                    c1.componentInputs.push_back(verilogConnections->at(count));
                }
                else if(verilogConnections->at(count).Name.compare(tempStr3) == 0)
                {
                    c1.componentInputs.push_back(verilogConnections->at(count));
                }
            }
            verilogComponents->push_back(c1);
            compcountshr++;//Frank's class stuff uses this NOT DELETE
            shr.shrcount(compcountshr);//Frank's class stuff uses this NOT DELETE
            if(tempStr.size()!=0)//Frank's class stuff uses this NOT DELETE
            {
                connectionCOMP.connectionSHRtrue(1);//Frank's class stuff uses this NOT DELETE
            }
        }
        // SHL
        else if(line.find("<<") != std::string::npos)
        {
            c1.compName = "SHL";
            while((beg = line.find_first_not_of(delims, pos)) != std::string::npos)
            {
                pos = line.find_first_of(delims, beg+1);
                strings.push_back(line.substr(beg, pos-beg));
            }
            tempStr = strings.at(0);
            tempStr2 = strings.at(2);
            tempStr3 = strings.at(4);
            for(count = 0; count < verilogConnections->size(); count++)
            {
                if(verilogConnections->at(count).Name.compare(tempStr) == 0)
                {
                    tempSize = verilogConnections->at(count).getSize();
                    c1.size = tempSize;
                    c1.latency = setLatencyValues(tempSize, c1.compName);
                    c1.componentOutputs.push_back(verilogConnections->at(count));
                }
                else if(verilogConnections->at(count).Name.compare(tempStr2) == 0)
                {
                    c1.componentInputs.push_back(verilogConnections->at(count));
                }
                else if(verilogConnections->at(count).Name.compare(tempStr3) == 0)
                {
                    c1.componentInputs.push_back(verilogConnections->at(count));
                }
            }
            verilogComponents->push_back(c1);
            compcountshl++;//Frank's class stuff uses this NOT DELETE
            shl.shlcount(compcountshl);//Frank's class stuff uses this NOT DELETE
            if(tempStr.size()!=0)//Frank's class stuff uses this NOT DELETE
            {
                connectionCOMP.connectionSHLtrue(1);//Frank's class stuff uses this NOT DELETE
            }
        }
        // DIV
        else if(line.find("/") != std::string::npos)
        {
            c1.compName = "DIV";
            while((beg = line.find_first_not_of(delims, pos)) != std::string::npos)
            {
                pos = line.find_first_of(delims, beg+1);
                strings.push_back(line.substr(beg, pos-beg));
            }
            tempStr = strings.at(0);
            tempStr2 = strings.at(2);
            tempStr3 = strings.at(4);
            for(count = 0; count < verilogConnections->size(); count++)
            {
                if(verilogConnections->at(count).Name.compare(tempStr) == 0)
                {
                    tempSize = verilogConnections->at(count).getSize();
                    c1.size = tempSize;
                    c1.latency = setLatencyValues(tempSize, c1.compName);
                    c1.componentOutputs.push_back(verilogConnections->at(count));
                }
                else if(verilogConnections->at(count).Name.compare(tempStr2) == 0)
                {
                    c1.componentInputs.push_back(verilogConnections->at(count));
                }
                else if(verilogConnections->at(count).Name.compare(tempStr3) == 0)
                {
                    c1.componentInputs.push_back(verilogConnections->at(count));
                }
            }
            verilogComponents->push_back(c1);
            compcountdiv++;//Frank's class stuff uses this NOT DELETE
            div1.divcount(compcountdiv);//Frank's class stuff uses this NOT DELETE
            if(tempStr.size()!=0)//Frank's class stuff uses this NOT DELETE
            {
                connectionCOMP.connectionDIVtrue(1);//Frank's class stuff uses this NOT DELETE
            }
        }
        // MOD
        else if(line.find("%") != std::string::npos)
        {
            c1.compName = "MOD";
            while((beg = line.find_first_not_of(delims, pos)) != std::string::npos)
            {
                pos = line.find_first_of(delims, beg+1);
                strings.push_back(line.substr(beg, pos-beg));
            }
            tempStr = strings.at(0);
            tempStr2 = strings.at(2);
            tempStr3 = strings.at(4);
            for(count = 0; count < verilogConnections->size(); count++)
            {
                if(verilogConnections->at(count).Name.compare(tempStr) == 0)
                {
                    tempSize = verilogConnections->at(count).getSize();
                    c1.size = tempSize;
                    c1.latency = setLatencyValues(tempSize, c1.compName);
                    c1.componentOutputs.push_back(verilogConnections->at(count));
                }
                else if(verilogConnections->at(count).Name.compare(tempStr2) == 0)
                {
                    c1.componentInputs.push_back(verilogConnections->at(count));
                }
                else if(verilogConnections->at(count).Name.compare(tempStr3) == 0)
                {
                    c1.componentInputs.push_back(verilogConnections->at(count));
                }
            }
            verilogComponents->push_back(c1);
            compcountmod++;//Frank's class stuff uses this NOT DELETE
            mod.modcount(compcountmod);//Frank's class stuff uses this NOT DELETE
            if(tempStr.size()!=0)//Frank's class stuff uses this NOT DELETE
            {
                connectionCOMP.connectionMODtrue(1);//Frank's class stuff uses this NOT DELETE
            }
        }
        // INC
        else if(line.find("+ 1") != std::string::npos)
        {
            c1.compName = "INC";
            while((beg = line.find_first_not_of(delims, pos)) != std::string::npos)
            {
                pos = line.find_first_of(delims, beg+1);
                strings.push_back(line.substr(beg, pos-beg));
            }
            tempStr = strings.at(0);
            tempStr2 = strings.at(2);
            for(count = 0; count < verilogConnections->size(); count++)
            {
                if(verilogConnections->at(count).Name.compare(tempStr) == 0)
                {
                    tempSize = verilogConnections->at(count).getSize();
                    c1.size = tempSize;
                    c1.latency = setLatencyValues(tempSize, c1.compName);
                    c1.componentOutputs.push_back(verilogConnections->at(count));
                }
                else if(verilogConnections->at(count).Name.compare(tempStr2) == 0)
                {
                    c1.componentInputs.push_back(verilogConnections->at(count));
                }

            }
            // Create Wire for 1
            connectionType connection;
            connection.Size = "1";
            connection.Name = "1";
            connection.output = false;
            connection.input = false;
            connection.wire = true;
            verilogConnections->push_back(connection);
            c1.componentInputs.push_back(connection);
            verilogComponents->push_back(c1);

            compcountinc++;//Frank's class stuff uses this NOT DELETE
            inc.inccount(compcountinc);//Frank's class stuff uses this NOT DELETE
            if(tempStr.size()!=0)//Frank's class stuff uses this NOT DELETE
            {
                connectionCOMP.connectionMODtrue(1);//Frank's class stuff uses this NOT DELETE
            }
        }
        // DEC
        else if(line.find("- 1") != std::string::npos)
        {
            c1.compName = "DEC";
            while((beg = line.find_first_not_of(delims, pos)) != std::string::npos)
            {
                pos = line.find_first_of(delims, beg+1);
                strings.push_back(line.substr(beg, pos-beg));
            }
            tempStr = strings.at(0);
            tempStr2 = strings.at(2);
            for(count = 0; count < verilogConnections->size(); count++)
            {
                if(verilogConnections->at(count).Name.compare(tempStr) == 0)
                {
                    tempSize = verilogConnections->at(count).getSize();
                    c1.size = tempSize;
                    c1.latency = setLatencyValues(tempSize, c1.compName);
                    c1.componentOutputs.push_back(verilogConnections->at(count));
                }
                else if(verilogConnections->at(count).Name.compare(tempStr2) == 0)
                {
                    c1.componentInputs.push_back(verilogConnections->at(count));
                }

            }
            // Create Wire for 1
            connectionType connection;
            connection.Size = "1";
            connection.Name = "1";
            connection.output = false;
            connection.input = false;
            connection.wire = true;
            verilogConnections->push_back(connection);
            c1.componentInputs.push_back(connection);
            verilogComponents->push_back(c1);

            compcountdec++;//Frank's class stuff uses this NOT DELETE
            dec.deccount(compcountdec);//Frank's class stuff uses this NOT DELETE
            if(tempStr.size()!=0)//Frank's class stuff uses this NOT DELETE
            {
                connectionCOMP.connectionDECtrue(1);//Frank's class stuff uses this NOT DELETE
            }
        }
        // REG
        else if (line.find("=") != std::string::npos)
        {
            c1.compName = "REG";
            while((beg = line.find_first_not_of(delims, pos)) != std::string::npos)
            {
                pos = line.find_first_of(delims, beg+1);
                strings.push_back(line.substr(beg, pos-beg));
            }
            tempStr = strings.at(0);
            tempStr2 = strings.at(2);
            for(count = 0; count < verilogConnections->size(); count++)
            {
                if(verilogConnections->at(count).Name.compare(tempStr) == 0)
                {
                    tempSize = verilogConnections->at(count).getSize();
                    c1.size = tempSize;
                    c1.latency = setLatencyValues(tempSize, c1.compName);
                    c1.componentOutputs.push_back(verilogConnections->at(count));
                }
                else if(verilogConnections->at(count).Name.compare(tempStr2) == 0)
                {
                    c1.componentInputs.push_back(verilogConnections->at(count));
                }

                else if(verilogConnections->at(count).Name.compare("Clk"))
                {
                    c1.componentInputs.push_back(verilogConnections->at(count));
                }

                else if(verilogConnections->at(count).Name.compare("Rst"))
                {
                    c1.componentInputs.push_back(verilogConnections->at(count));
                }

            }
            verilogComponents->push_back(c1);
            compcountreg++;//Frank's class stuff uses this NOT DELETE
            reg.regcount(compcountreg);//Frank's class stuff uses this NOT DELETE
            if(tempStr.size()!=0)//Frank's class stuff uses this NOT DELETE
            {
                connectionCOMP.connectionREGtrue(1);//Frank's class stuff uses this NOT DELETE
            }

        }
    }
}







