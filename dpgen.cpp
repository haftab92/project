//
//  dpgen.cpp
//  Datapath Generator
//
//  Created by Brady Thomas on 10/28/20.
//  MODIFIED BY FRANK P. las update 11/16/2020 @ 8:35PM COMPILES FINE
//  FRank P my modifications here a basically FLAGS
//

#include "dpgen.hpp"
#include "datatypes.hpp"
#include <iostream>
#include <fstream>
#include <string>
#include <vector>




double setLatencyValues(std::string& componentSize, std::string& componentName) {
    double latency = 0;
    dataType data;

    // Components with 1 bit size
    if(componentSize.compare("1") == 0) {
        if(componentName.compare("ADD") == 0) {
            latency = data.add1b;
            booleanbit booleanadd1;         //Frank's class stuff uses this NOT DELETE
            booleanadd1.adder1btrue(1);     //Frank's class stuff uses this NOT DELETE
        }

        else if(componentName.compare("REG") == 0) {
            latency = data.reg1b;
            booleanbit booleanreg;  //Frank's class stuff uses this NOT DELETE
            booleanreg.reg1true(1); //Frank's class stuff uses this NOT DELETE
        }

        else if(componentName.compare("SUB") == 0) {
            latency = data.sub1b;
            booleanbit booleansub;  //Frank's class stuff uses this NOT DELETE
            booleansub.sub1true(1); //Frank's class stuff uses this NOT DELETE
        }

        else if(componentName.compare("MUL") == 0) {
            latency = data.mult1b;
            booleanbit booleamul; //Frank's class stuff uses this NOT DELETE
            booleamul.mul1true(1);//Frank's class stuff uses this NOT DELETE
        }

        else if(componentName.compare("COMP") == 0) {
            latency = data.comp1b;
            booleanbit booleancomp; //Frank's class stuff uses this NOT DELETE
            booleancomp.comp1true(1); //Frank's class stuff uses this NOT DELETE
        }

        else if(componentName.compare("MUX2x1") == 0) {
            latency = data.mux1b;
            booleanbit booleanmux; //Frank's class stuff uses this NOT DELETE
            booleanmux.mux1true(1); //Frank's class stuff uses this NOT DELETE
        }

        else if(componentName.compare("SHR") == 0) {
            latency = data.shr1b;
            booleanbit booleanshr; //Frank's class stuff uses this NOT DELETE
            booleanshr.shr1true(1); //Frank's class stuff uses this NOT DELETE
        }

        else if(componentName.compare("SHL") == 0) {
            latency = data.shl1b;
            booleanbit booleanshl; //Frank's class stuff uses this NOT DELETE
            booleanshl.shl1true(1); //Frank's class stuff uses this NOT DELETE
        }

        else if(componentName.compare("DIV") == 0) {
            latency = data.div1b;
            booleanbit booleandiv; //Frank's class stuff uses this NOT DELETE
            booleandiv.div1true(1); //Frank's class stuff uses this NOT DELETE
        }

        else if(componentName.compare("MOD") == 0) {
            latency = data.mod1b;
            booleanbit booleanmod; //Frank's class stuff uses this NOT DELETE
            booleanmod.mod1true(1); //Frank's class stuff uses this NOT DELETE
        }

        else if(componentName.compare("INC") == 0) {
            latency = data.inc1b;
            booleanbit booleanInc; //Frank's class stuff uses this NOT DELETE
            booleanInc.inc1true(1); //Frank's class stuff uses this NOT DELETE
        }

        else if(componentName.compare("DEC") == 0) {
            latency = data.dec1b;
            booleanbit booleandec; //Frank's class stuff uses this NOT DELETE
            booleandec.dec1true(1); //Frank's class stuff uses this NOT DELETE
        }

        else {
            std::cout << "Invalid data" << std::endl;
        }

    }

    // Components with bit size 2
    else if(componentSize.compare("2") == 0) {
        if(componentName.compare("ADD") == 0) {
            latency = data.add2b;
            booleanbit booleanadd2; //Frank's class stuff uses this NOT DELETE
            booleanadd2.adder2btrue(1);//Frank's class stuff uses this NOT DELETE
        }

        else if(componentName.compare("REG") == 0) {
            latency = data.reg2b;
            booleanbit booleanreg; //Frank's class stuff uses this NOT DELETE
            booleanreg.reg2true(1); //Frank's class stuff uses this NOT DELETE
        }

        else if(componentName.compare("SUB") == 0) {
            latency = data.sub2b;
            booleanbit booleansub; //Frank's class stuff uses this NOT DELETE
            booleansub.sub2true(1); //Frank's class stuff uses this NOT DELETE
        }

        else if(componentName.compare("MUL") == 0) {
            latency = data.mult2b;
            booleanbit booleamul; //Frank's class stuff uses this NOT DELETE
            booleamul.mul2true(1); //Frank's class stuff uses this NOT DELETE
        }


        else if(componentName.compare("COMP") == 0) {
            latency = data.comp2b;
            booleanbit booleancomp; //Frank's class stuff uses this NOT DELETE
            booleancomp.comp2true(1); //Frank's class stuff uses this NOT DELETE
        }

        else if(componentName.compare("MUX2x1") == 0) {
            latency = data.mux2b;
            booleanbit booleanmux; //Frank's class stuff uses this NOT DELETE
            booleanmux.mux2true(1); //Frank's class stuff uses this NOT DELETE
        }

        else if(componentName.compare("SHR") == 0) {
            latency = data.shr2b;
            booleanbit booleanshr; //Frank's class stuff uses this NOT DELETE
            booleanshr.shr2true(1); //Frank's class stuff uses this NOT DELETE
        }

        else if(componentName.compare("SHL") == 0) {
            latency = data.shl2b;
            booleanbit booleanshl; //Frank's class stuff uses this NOT DELETE
            booleanshl.shl2true(1); //Frank's class stuff uses this NOT DELETE
        }

        else if(componentName.compare("DIV") == 0) {
            latency = data.div2b;
            booleanbit booleandiv; //Frank's class stuff uses this NOT DELETE
            booleandiv.div2true(1); //Frank's class stuff uses this NOT DELETE
        }

        else if(componentName.compare("MOD") == 0) {
            latency = data.mod2b;
            booleanbit booleanmod; //Frank's class stuff uses this NOT DELETE
            booleanmod.mod2true(1); //Frank's class stuff uses this NOT DELETE
        }

        else if(componentName.compare("INC") == 0) {
            latency = data.inc2b;
            booleanbit booleanInc; //Frank's class stuff uses this NOT DELETE
            booleanInc.inc2true(1); //Frank's class stuff uses this NOT DELETE
        }

        else if(componentName.compare("DEC") == 0) {
            latency = data.dec2b;
            booleanbit booleandec; //Frank's class stuff uses this NOT DELETE
            booleandec.dec2true(1); //Frank's class stuff uses this NOT DELETE
        }

        else {
            std::cout << "Invalid data" << std::endl;
        }

    }

    else if(componentSize.compare("8") == 0) {
        if(componentName.compare("ADD") == 0) {
            latency = data.add8b;
            booleanbit booleanadd8; //Frank's class stuff uses this NOT DELETE
            booleanadd8.adder8btrue(1); //Frank's class stuff uses this NOT DELETE
        }

        else if(componentName.compare("REG") == 0) {
            latency = data.reg8b;
            booleanbit booleanreg; //Frank's class stuff uses this NOT DELETE
            booleanreg.reg8true(1); //Frank's class stuff uses this NOT DELETE
        }

        else if(componentName.compare("SUB") == 0) {
            latency = data.sub8b;
            booleanbit booleansub; //Frank's class stuff uses this NOT DELETE
            booleansub.sub8true(1); //Frank's class stuff uses this NOT DELETE
        }

        else if(componentName.compare("MUL") == 0) {
            latency = data.mult8b;
            booleanbit booleamul; //Frank's class stuff uses this NOT DELETE
            booleamul.mul8true(1); //Frank's class stuff uses this NOT DELETE
        }

        else if(componentName.compare("COMP") == 0) {
            latency = data.comp8b;
            booleanbit booleancomp; //Frank's class stuff uses this NOT DELETE
            booleancomp.comp8true(1); //Frank's class stuff uses this NOT DELETE
        }

        else if(componentName.compare("MUX2x1") == 0) {
            latency = data.mux8b;
            booleanbit booleanmux; //Frank's class stuff uses this NOT DELETE
            booleanmux.mux8true(1); //Frank's class stuff uses this NOT DELETE
        }

        else if(componentName.compare("SHR") == 0) {
            latency = data.shr8b;
            booleanbit booleanshr; //Frank's class stuff uses this NOT DELETE
            booleanshr.shr8true(1); //Frank's class stuff uses this NOT DELETE
        }

        else if(componentName.compare("SHL") == 0) {
            latency = data.shl8b;
            booleanbit booleanshl; //Frank's class stuff uses this NOT DELETE
            booleanshl.shl8true(1); //Frank's class stuff uses this NOT DELETE
        }

        else if(componentName.compare("DIV") == 0) {
            latency = data.div8b;
            booleanbit booleandiv; //Frank's class stuff uses this NOT DELETE
            booleandiv.div8true(1); //Frank's class stuff uses this NOT DELETE
        }

        else if(componentName.compare("MOD") == 0) {
            latency = data.mod8b;
            booleanbit booleanmod; //Frank's class stuff uses this NOT DELETE
            booleanmod.mod8true(1); //Frank's class stuff uses this NOT DELETE
        }

        else if(componentName.compare("INC") == 0) {
            latency = data.inc8b;
            booleanbit booleanInc; //Frank's class stuff uses this NOT DELETE
            booleanInc.inc8true(1); //Frank's class stuff uses this NOT DELETE
        }

        else if(componentName.compare("DEC") == 0) {
            latency = data.dec8b;
            booleanbit booleandec; //Frank's class stuff uses this NOT DELETE
            booleandec.dec8true(1); //Frank's class stuff uses this NOT DELETE
        }

        else {
            std::cout << "Invalid data" << std::endl;
        }

    }

    else if(componentSize.compare("16") == 0) {
        if(componentName.compare("ADD") == 0) {
            latency = data.add16b;
            booleanbit booleanadd16; //Frank's class stuff uses this NOT DELETE
            booleanadd16.adder16btrue(1); //Frank's class stuff uses this NOT DELETE
        }

        else if(componentName.compare("REG") == 0) {
            latency = data.reg16b;
            booleanbit booleanreg; //Frank's class stuff uses this NOT DELETE
            booleanreg.reg16true(1); //Frank's class stuff uses this NOT DELETE
        }

        else if(componentName.compare("SUB") == 0) {
            latency = data.sub16b;
            booleanbit booleansub; //Frank's class stuff uses this NOT DELETE
            booleansub.sub16true(1); //Frank's class stuff uses this NOT DELETE
        }

        else if(componentName.compare("MUL") == 0) {
            latency = data.mult16b;
            booleanbit booleamul; //Frank's class stuff uses this NOT DELETE
            booleamul.mul16true(1); //Frank's class stuff uses this NOT DELETE
        }

        else if(componentName.compare("COMP") == 0) {
            latency = data.comp16b;
            booleanbit booleancomp; //Frank's class stuff uses this NOT DELETE
            booleancomp.comp16true(1);//Frank's class stuff uses this NOT DELETE
        }

        else if(componentName.compare("MUX2x1") == 0) {
            latency = data.mux16b;
            booleanbit booleanmux; //Frank's class stuff uses this NOT DELETE
            booleanmux.mux16true(1); //Frank's class stuff uses this NOT DELETE
        }

        else if(componentName.compare("SHR") == 0) {
            latency = data.shr16b;
            booleanbit booleanshr; //Frank's class stuff uses this NOT DELETE
            booleanshr.shr16true(1); //Frank's class stuff uses this NOT DELETE
        }

        else if(componentName.compare("SHL") == 0) {
            latency = data.shl16b;
            booleanbit booleanshl; //Frank's class stuff uses this NOT DELETE
            booleanshl.shl16true(1); //Frank's class stuff uses this NOT DELETE
        }

        else if(componentName.compare("DIV") == 0) {
            latency = data.div16b;
            booleanbit booleandiv; //Frank's class stuff uses this NOT DELETE
            booleandiv.div16true(1); //Frank's class stuff uses this NOT DELETE
        }

        else if(componentName.compare("MOD") == 0) {
            latency = data.mod16b;
            booleanbit booleanmod; //Frank's class stuff uses this NOT DELETE
            booleanmod.mod16true(1); //Frank's class stuff uses this NOT DELETE
        }

        else if(componentName.compare("INC") == 0) {
            latency = data.inc16b;
            booleanbit booleanInc; //Frank's class stuff uses this NOT DELETE
            booleanInc.inc16true(1); //Frank's class stuff uses this NOT DELETE
        }

        else if(componentName.compare("DEC") == 0) {
            latency = data.dec16b;
            booleanbit booleandec; //Frank's class stuff uses this NOT DELETE
            booleandec.dec16true(1); //Frank's class stuff uses this NOT DELETE
        }

        else {
            std::cout << "Invalid data" << std::endl;
        }

    }

    else if(componentSize.compare("32") == 0) {
        if(componentName.compare("ADD") == 0) {
            latency = data.add32b;
            booleanbit booleanadd32; //Frank's class stuff uses this NOT DELETE
            booleanadd32.adder32btrue(1); //Frank's class stuff uses this NOT DELETE
        }

        else if(componentName.compare("REG") == 0) {
            latency = data.reg32b;
            booleanbit booleanreg; //Frank's class stuff uses this NOT DELETE
            booleanreg.reg32true(1);//Frank's class stuff uses this NOT DELETE
        }

        else if(componentName.compare("SUB") == 0) {
            latency = data.sub32b;
            booleanbit booleansub; //Frank's class stuff uses this NOT DELETE
            booleansub.sub32true(1); //Frank's class stuff uses this NOT DELETE
        }

        else if(componentName.compare("MUL") == 0) {
            latency = data.mult32b;
            booleanbit booleamul; //Frank's class stuff uses this NOT DELETE
            booleamul.mul32rue(1); //Frank's class stuff uses this NOT DELETE
        }

        else if(componentName.compare("COMP") == 0) {
            latency = data.comp32b;
            booleanbit booleancomp; //Frank's class stuff uses this NOT DELETE
            booleancomp.comp32true(1); //Frank's class stuff uses this NOT DELETE
        }

        else if(componentName.compare("MUX2x1") == 0) {
            latency = data.mux32b;
            booleanbit booleanmux; //Frank's class stuff uses this NOT DELETE
            booleanmux.mux32true(1); //Frank's class stuff uses this NOT DELETE
        }

        else if(componentName.compare("SHR") == 0) {
            latency = data.shr32b;
            booleanbit booleanshr; //Frank's class stuff uses this NOT DELETE
            booleanshr.shr32true(1); //Frank's class stuff uses this NOT DELETE
        }

        else if(componentName.compare("SHL") == 0) {
            latency = data.shl32b;
            booleanbit booleanshl; //Frank's class stuff uses this NOT DELETE
            booleanshl.shlt32rue(1); //Frank's class stuff uses this NOT DELETE
        }

        else if(componentName.compare("DIV") == 0) {
            latency = data.div32b;
            booleanbit booleandiv; //Frank's class stuff uses this NOT DELETE
            booleandiv.div32true(1); //Frank's class stuff uses this NOT DELETE
        }

        else if(componentName.compare("MOD") == 0) {
            latency = data.mod32b;
            booleanbit booleanmod; //Frank's class stuff uses this NOT DELETE
            booleanmod.mod32true(1); //Frank's class stuff uses this NOT DELETE
        }

        else if(componentName.compare("INC") == 0) {
            latency = data.inc32b;
            booleanbit booleanInc; //Frank's class stuff uses this NOT DELETE
            booleanInc.inc32true(1); //Frank's class stuff uses this NOT DELETE
        }

        else if(componentName.compare("DEC") == 0) {
            latency = data.dec32b;
            booleanbit booleandec; //Frank's class stuff uses this NOT DELETE
            booleandec.dec32true(1); //Frank's class stuff uses this NOT DELETE
        }

        else {
            std::cout << "Invalid data" << std::endl;
        }

    }

    else if(componentSize.compare("64") == 0) {
        if(componentName.compare("ADD") == 0) {
            latency = data.add64b;
            booleanbit booleanadd64; //Frank's class stuff uses this NOT DELETE
            booleanadd64.adder64btrue(1); //Frank's class stuff uses this NOT DELETE
        }

        else if(componentName.compare("REG") == 0) {
            latency = data.reg64b;
            booleanbit booleanreg; //Frank's class stuff uses this NOT DELETE
            booleanreg.reg64true(1); //Frank's class stuff uses this NOT DELETE
        }

        else if(componentName.compare("SUB") == 0) {
            latency = data.sub64b;
            booleanbit booleansub; //Frank's class stuff uses this NOT DELETE
            booleansub.sub64true(1); //Frank's class stuff uses this NOT DELETE
        }

        else if(componentName.compare("MUL") == 0) {
            latency = data.mult64b;
            booleanbit booleamul; //Frank's class stuff uses this NOT DELETE
            booleamul.mul1true(1); //Frank's class stuff uses this NOT DELETE
        }

        else if(componentName.compare("COMP") == 0) {
            latency = data.comp64b;
            booleanbit booleancomp; //Frank's class stuff uses this NOT DELETE
            booleancomp.comp64true(1); //Frank's class stuff uses this NOT DELETE
        }

        else if(componentName.compare("MUX2x1") == 0) {
            latency = data.mux64b;
            booleanbit booleanmux; //Frank's class stuff uses this NOT DELETE
            booleanmux.mux64true(1); //Frank's class stuff uses this NOT DELETE
        }

        else if(componentName.compare("SHR") == 0) {
            latency = data.shr64b;
            booleanbit booleanshr; //Frank's class stuff uses this NOT DELETE
            booleanshr.shr64true(1); //Frank's class stuff uses this NOT DELETE
        }

        else if(componentName.compare("SHL") == 0) {
            latency = data.shl64b;
            booleanbit booleanshl; //Frank's class stuff uses this NOT DELETE
            booleanshl.shlt64rue(1);  //Frank's class stuff uses this NOT DELETE
        }

        else if(componentName.compare("DIV") == 0) {
            latency = data.div64b;
            booleanbit booleandiv; //Frank's class stuff uses this NOT DELETE
            booleandiv.div64true(1); //Frank's class stuff uses this NOT DELETE
        }

        else if(componentName.compare("MOD") == 0) {
            latency = data.mod64b;
            booleanbit booleanmod; //Frank's class stuff uses this NOT DELETE
            booleanmod.mod64true(1); //Frank's class stuff uses this NOT DELETE
        }

        else if(componentName.compare("INC") == 0) {
            latency = data.inc64b;
            booleanbit booleanInc; //Frank's class stuff uses this NOT DELETE
            booleanInc.inc64true(1); //Frank's class stuff uses this NOT DELETE

        }

        else if(componentName.compare("DEC") == 0) {
            latency = data.dec64b;
            booleanbit booleandec; //Frank's class stuff uses this NOT DELETE
            booleandec.dec64true(1); //Frank's class stuff uses this NOT DELETE
        }

        else {
            std::cout << "Invalid data" << std::endl;
        }

    }


    return latency;
}
