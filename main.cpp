//
//  main.cpp
//  Datapath Generator
//
//  Created by Brady Thomas on 10/28/20.
//
using namespace std;

#include <iostream>
#include <fstream>
#include "parser.hpp"
#include "graph.hpp"
#include "dpgen.hpp"
#include "datatypes.hpp"
#include <vector>
#include <string>

/* global variable */
string outputFile = "";



int main(int argc, char * argv[]) {
    parser p = new parser();
       
    if( argc == 3 ) {
    const string inputFile(argv[1]);
    outputFile = string(argv[2]); 
   
    p.parseFile(argv[1]);
    }
    else {
      printf("Incorrect number of arguments\n");
   }


}
